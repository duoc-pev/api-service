package cl.convenience.store.dao.query;

import cl.convenience.store.model.search.ProductSearch;

import static cl.convenience.store.util.Constant.ADD;
import static cl.convenience.store.util.Constant.AND;
import static cl.convenience.store.util.Constant.FROM;
import static cl.convenience.store.util.Constant.LEFT_OUTER_JOIN;
import static cl.convenience.store.util.Constant.SELECT;
import static cl.convenience.store.util.Constant.WHERE;

public class QueryProductsUtil {

    private QueryProductsUtil() {
        throw new IllegalAccessError(QueryProductsUtil.class.toString());
    }

    public static String getProductCategoryListSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectProductCategoryOnly());
        query.append(fromProductCategory());

        return query.toString();
    }

    public static String getProductCategoryByIdSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectProductCategoryOnly());
        query.append(fromProductCategory());
        query.append(WHERE);
        query.append(" pc.prod_cat_id = ? ");

        return query.toString();
    }

    public static String getProductUnitListSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectProductUnitOnly());
        query.append(fromProductUnit());

        return query.toString();
    }

    public static String getProductUnitByIdSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectProductUnitOnly());
        query.append(fromProductUnit());
        query.append(WHERE);
        query.append(" pu.prod_un_id = ? ");

        return query.toString();
    }

    public static String getProductBrandListSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectProductBrandsOnly());
        query.append(fromProductBrands());

        return query.toString();
    }

    public static String getProductBrandByIdSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectProductBrandsOnly());
        query.append(fromProductBrands());
        query.append(WHERE);
        query.append(" br.brand_id = ? ");

        return query.toString();
    }

    public static String getProductListSql(ProductSearch search) {
        StringBuilder query = new StringBuilder();

        query.append(selectProducts());
        query.append(searchWhereProduct(search));

        return query.toString();
    }

    public static String getProductByIdSql() {
        StringBuilder query = new StringBuilder();

        query.append(selectProducts());
        query.append(WHERE);
        query.append(" pro.product_id = ? ");

        return query.toString();
    }

    public static String getInventoryByProductIdSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectInventoryOnly());
        query.append(fromInventory());
        query.append(whereInventory());

        return query.toString();
    }

    private static StringBuilder selectProducts() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectProductsOnly());
        query.append(ADD);
        query.append(selectProductBrandsOnly());
        query.append(ADD);
        query.append(selectProductCategoryOnly());
        query.append(ADD);
        query.append(selectProductUnitOnly());
        query.append(ADD);
        query.append(selectProductPriceOnly());
        query.append(fromProduct());

        return query;
    }

    private static StringBuilder selectProductCategoryOnly() {
        StringBuilder query = new StringBuilder();

        query.append("pc.prod_cat_id, ");
        query.append("pc.prod_cat_name, ");
        query.append("pc.prod_cat_desc, ");
        query.append("pc.prod_cat_parent_id ");

        return query;
    }

    private static StringBuilder fromProductCategory() {
        StringBuilder query = new StringBuilder();

        query.append(FROM);
        query.append(" prod_category pc ");

        return query;
    }

    private static StringBuilder selectProductUnitOnly() {
        StringBuilder query = new StringBuilder();

        query.append("pu.prod_un_id, ");
        query.append("pu.prod_un_name, ");
        query.append("pu.prod_un_desc ");

        return query;
    }

    private static StringBuilder fromProductUnit() {
        StringBuilder query = new StringBuilder();

        query.append(FROM);
        query.append(" prod_unit pu ");

        return query;
    }

    private static StringBuilder selectProductBrandsOnly() {
        StringBuilder query = new StringBuilder();

        query.append("br.brand_id, ");
        query.append("br.brand_sname, ");
        query.append("br.brand_fname ");

        return query;
    }

    private static StringBuilder fromProductBrands() {
        StringBuilder query = new StringBuilder();

        query.append(FROM);
        query.append(" brands br ");

        return query;
    }

    private static StringBuilder selectProductsOnly() {
        StringBuilder query = new StringBuilder();

        query.append(" distinct ");
        query.append("pro.product_id, ");
        query.append("pro.product_sku, ");
        query.append("pro.product_name, ");
        query.append("pro.product_description, ");
        query.append("pro.sup_id ");

        return query;
    }

    private static StringBuilder selectProductPriceOnly() {
        StringBuilder query = new StringBuilder();

        query.append("pp.pp_id, ");
        query.append("pp.pp_product_id, ");
        query.append("pp.pp_full_price, ");
        query.append("pp.pp_sell_net_price, ");
        query.append("pp.pp_buy_net_price, ");
        query.append("pp.pp_sell_tax_price, ");
        query.append("pp.pp_buy_tax_price, ");
        query.append("round(extract(second from pp.pp_base_created) + (trunc(pp.pp_base_created, 'mi') - date '1970-01-01') * (24 * 60 * 60) * 1000) as pp_base_created, ");
        query.append("round(extract(second from pp.pp_base_expired) + (trunc(pp.pp_base_expired, 'mi') - date '1970-01-01') * (24 * 60 * 60) * 1000) as pp_base_expired ");

        return query;
    }

    private static StringBuilder fromProduct() {
        StringBuilder query = new StringBuilder();

        query.append(FROM);
        query.append(" products pro ");
        query.append(LEFT_OUTER_JOIN);
        query.append(" brands br on (br.brand_id = pro.brand_id) ");
        query.append(LEFT_OUTER_JOIN);
        query.append(" prod_category pc on (pc.prod_cat_id = pro.prod_cat_id) ");
        query.append(LEFT_OUTER_JOIN);
        query.append(" prod_unit pu on (pu.prod_un_id = pro.prod_un_id) ");
        query.append(LEFT_OUTER_JOIN);
        query.append(" product_price pp on (pp.pp_product_id = pro.product_id) ");
        query.append(LEFT_OUTER_JOIN);
        query.append(" BRANCH_INVENTORY bi on (bi.BI_PRODUCT_ID = pro.PRODUCT_ID) ");


        return query;
    }

    private static StringBuilder searchWhereProduct(ProductSearch search) {
        StringBuilder where = new StringBuilder();

        where.append(WHERE);
        where.append(" pro.product_id > 0 ");

        if (null != search.getProduct().getSku()) {
            where.append(AND);
            where.append(" pro.product_sku LIKE '").append(search.getProduct().getSku()).append("%' ");
        }

        if (null != search.getProduct().getName()) {
            where.append(AND);
            where.append(" pro.product_name LIKE '").append(search.getProduct().getName()).append("%' ");
        }

        if (null != search.getProduct().getBrand().getShortName()) {
            where.append(AND);
            where.append(" br.brand_sname LIKE '").append(search.getProduct().getBrand().getShortName()).append("%' ");
        }

        if (null != search.getProduct().getCategory().getId()) {
            where.append(AND);
            where.append(" pc.prod_cat_id = ").append(search.getProduct().getCategory().getId());
        }

        if (null != search.getProduct().getUnit().getId()) {
            where.append(AND);
            where.append(" pu.prod_un_id = ").append(search.getProduct().getUnit().getId());
        }

        where.append(" order by pro.PRODUCT_ID asc ");

        return where;
    }

    private static StringBuilder selectInventoryOnly() {
        StringBuilder query = new StringBuilder();

        query.append("bi.BI_ID, ");
        query.append("bi.BI_BRANCH_ID, ");
        query.append("bi.BI_PRODUCT_ID, ");
        query.append("bi.BI_QUANTITY, ");
        query.append("bi.BI_CRITICAL_Q, ");
        query.append("bi.BI_PRODUCT_CODE, ");
        query.append("round(extract(second from bi.BI_PRODUCT_EXPIRE) + (trunc(bi.BI_PRODUCT_EXPIRE, 'mi') - date '1970-01-01') * (24 * 60 * 60) * 1000) as BI_PRODUCT_EXPIRE ");

        return query;
    }

    private static StringBuilder fromInventory() {
        StringBuilder query = new StringBuilder();

        query.append(FROM);
        query.append(" branch_inventory bi ");

        return query;
    }

    private static StringBuilder whereInventory() {
        StringBuilder where = new StringBuilder();

        where.append(WHERE);
        where.append(" bi.bi_product_id = ? ");

        return where;
    }
}
