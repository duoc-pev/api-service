package cl.convenience.store.service.business.employee;

import cl.convenience.store.model.commons.Count;
import cl.convenience.store.model.commons.address.Address;
import cl.convenience.store.model.commons.person.Gender;
import cl.convenience.store.model.commons.person.Phone;
import cl.convenience.store.model.employee.BranchOffice;
import cl.convenience.store.model.employee.Employee;
import cl.convenience.store.model.employee.EmployeeResponse;
import cl.convenience.store.model.employee.Role;
import cl.convenience.store.model.search.AddressSearch;
import cl.convenience.store.model.search.EmployeeSearch;
import cl.convenience.store.model.search.PhoneSearch;
import cl.convenience.store.service.crud.commons.CommonsServiceCRUD;
import cl.convenience.store.service.crud.employee.EmployeeServiceCRUD;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeServiceCRUD employeeServiceCRUD;
    private final CommonsServiceCRUD commonsServiceCRUD;

    public EmployeeServiceImpl(EmployeeServiceCRUD employeeServiceCRUD, CommonsServiceCRUD commonsServiceCRUD) {
        this.commonsServiceCRUD = commonsServiceCRUD;
        this.employeeServiceCRUD = employeeServiceCRUD;
    }

    @Override
    public Future<EmployeeResponse> searchEmployees(EmployeeSearch search) {
        Future<EmployeeResponse> future = Future.future();

        EmployeeResponse response = new EmployeeResponse();

        this.employeeServiceCRUD.searchEmployee(search).compose(employees -> {
            Future<List<Employee>> employeeWithRole = Future.future();

            employeeServiceCRUD.getRoleList().compose(roles -> {
                employees.forEach(employee -> employee.setRole(roles.stream().filter(r -> r.getRoleId().equals(employee.getRole().getRoleId())).findFirst().orElse(null)));

                List<Employee> filteredList = employees.stream().filter(employee -> Objects.nonNull(employee.getRole())).collect(Collectors.toList());

                employeeWithRole.complete(filteredList);
            }, employeeWithRole);

            return employeeWithRole;

        }).compose(employees -> {
            Future<List<Employee>> employeesWithAddress = Future.future();

            if (CollectionUtils.isNotEmpty(employees)) {
                AddressSearch addressSearch = new AddressSearch(search.getEmployee().getAddress());

                commonsServiceCRUD.searchAddress(addressSearch).compose(addresses -> {
                    employees.forEach(employee -> {
                        addressSearch.getAddress().setId(employee.getAddress().getId());
                        employee.setAddress(addresses.stream().filter(c -> c.getId().equals(employee.getAddress().getId())).findFirst().orElse(null));
                    });

                    List<Employee> filteredList = employees.stream().filter(employee -> Objects.nonNull(employee.getAddress())).collect(Collectors.toList());
                    employeesWithAddress.complete(filteredList);
                }, employeesWithAddress);
            }

            return employeesWithAddress;
        }).compose(employeesWithAddress -> {
            Future<List<Employee>> employeesWithGender = Future.future();

            if ( CollectionUtils.isNotEmpty(employeesWithAddress)) {
                commonsServiceCRUD.getGenderList().compose(genders -> {
                    employeesWithAddress.forEach(customer -> customer.setGender(genders.stream().filter(g -> g.getId().equals(customer.getGender().getId())).findFirst().orElse(null)));
                    employeesWithGender.complete(employeesWithAddress);
                }, employeesWithGender);
            }

            return employeesWithGender;
        }).compose(employeesWithGender -> {
            Future<List<Employee>> employeesWithPhones = Future.future();

            if (CollectionUtils.isNotEmpty(employeesWithGender)) {
                PhoneSearch phoneSearch = new PhoneSearch(search.getEmployee().getPhone());

                commonsServiceCRUD.searchPhones(phoneSearch).compose(phones -> {
                    employeesWithGender.forEach(employee -> {
                        phoneSearch.getPhone().setPhoneId(employee.getPhone().getPhoneId());
                        employee.setPhone(phones.stream().filter(p -> p.getPhoneId().equals(employee.getPhone().getPhoneId())).findFirst().orElse(null));
                    });
                    List<Employee> filteredList = employeesWithGender.stream().filter(employee -> Objects.nonNull(employee.getPhone())).collect(Collectors.toList());
                    employeesWithPhones.complete(filteredList);
                }, employeesWithPhones);
            }

            return employeesWithPhones;
        }).compose(employeesWithPhones -> {
            Future<List<Employee>> employeesWithBranchFuture = Future.future();
            Future<List<BranchOffice>> branchListFuture = Future.future();
            Future<List<Address>> branchAddressesFuture = Future.future();
            Future<List<Phone>> branchPhonesFuture = Future.future();

            this.employeeServiceCRUD.getBranchList().setHandler(branchListFuture.completer());
            this.commonsServiceCRUD.searchAddress(new AddressSearch(new Address())).setHandler(branchAddressesFuture.completer());
            this.commonsServiceCRUD.searchPhones(new PhoneSearch(new Phone())).setHandler(branchPhonesFuture.completer());

            CompositeFuture.all(branchListFuture, branchAddressesFuture, branchPhonesFuture).compose(event -> {
                employeesWithPhones.forEach(employee -> {
                    employee.setBranchOffice(branchListFuture.result().stream().filter(b -> b.getId().equals(employee.getBranchOffice().getId())).findFirst().orElse(null));
                    employee.getBranchOffice().setAddress(branchAddressesFuture.result().stream().filter(a -> a.getId().equals(employee.getBranchOffice().getAddress().getId())).findFirst().orElse(null));
                    employee.getBranchOffice().setPhone(branchPhonesFuture.result().stream().filter(p -> p.getPhoneId().equals(employee.getBranchOffice().getPhone().getPhoneId())).findFirst().orElse(null));
                });

                employeesWithBranchFuture.complete(employeesWithPhones);
            }, employeesWithBranchFuture);

            return employeesWithBranchFuture;

        }).compose(employeeListComplete -> {
            response.setCount(new Count(employeeListComplete.size()));

            if (null != search.getPagination()) {
                employeeListComplete = employeeListComplete.stream().skip(search.getPagination().getOffset()).limit(search.getPagination().getLimit()).collect(Collectors.toList());
            }

            response.setEmployees(employeeListComplete);

            future.complete(response);
        }, future);

        return future;
    }

    @Override
    public Future<Employee> getEmployeeById(String employeeId) {
        Future<Employee> future = Future.future();
        this.employeeServiceCRUD.getEmployeeById(employeeId).compose(employee -> {
            Future<Address> addressFuture = Future.future();
            this.commonsServiceCRUD.getAddressById(employee.getAddress().getId().toString()).setHandler(addressFuture.completer());
            Future<List<Role>> roleFuture = Future.future();
            this.employeeServiceCRUD.getRoleList().setHandler(roleFuture.completer());
            Future<Gender> genderFuture = Future.future();
            this.commonsServiceCRUD.getGenderById(employee.getGender().getId().toString()).setHandler(genderFuture.completer());
            Future<Phone> phoneFuture = Future.future();
            this.commonsServiceCRUD.getPhoneById(employee.getPhone().getPhoneId().toString()).setHandler(phoneFuture.completer());
            Future<BranchOffice> officeFuture = Future.future();

            this.employeeServiceCRUD.getBranchById(employee.getBranchOffice().getId().toString()).compose(branchOffice -> {
                Future<Address> branchAddressFuture = Future.future();
                Future<Phone> branchPhoneFuture = Future.future();

                this.commonsServiceCRUD.getAddressById(branchOffice.getAddress().getId().toString()).setHandler(branchAddressFuture.completer());
                this.commonsServiceCRUD.getPhoneById(branchOffice.getPhone().getPhoneId().toString()).setHandler(branchPhoneFuture.completer());

                CompositeFuture.all(branchAddressFuture, branchPhoneFuture).compose(result -> {
                    branchOffice.setAddress(branchAddressFuture.result());
                    branchOffice.setPhone(branchPhoneFuture.result());
                    officeFuture.complete(branchOffice);
                }, officeFuture);

            }, officeFuture);

            Future<CompositeFuture> compositeFuture = Future.future();

            CompositeFuture.all(addressFuture, roleFuture, genderFuture, phoneFuture, officeFuture).setHandler(compositeFuture.completer());

            compositeFuture.compose(event -> {
                employee.setAddress(addressFuture.result());
                employee.setRole(roleFuture.result().stream().filter(r -> r.getRoleId().equals(employee.getRole().getRoleId())).findFirst().orElse(null));
                employee.setGender(genderFuture.result());
                employee.setPhone(phoneFuture.result());
                employee.setBranchOffice(officeFuture.result());

                future.complete(employee);
            }, future);
        }, future);

        return future;
    }
}
