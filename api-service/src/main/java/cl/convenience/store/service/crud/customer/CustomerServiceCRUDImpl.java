package cl.convenience.store.service.crud.customer;

import cl.convenience.store.dao.CustomerDAO;
import cl.convenience.store.dao.JdbcRepositoryWrapper;
import cl.convenience.store.model.commons.person.Rut;
import cl.convenience.store.model.customer.Customer;
import cl.convenience.store.model.search.CustomerSearch;
import io.vertx.core.Future;

import java.util.List;

public class CustomerServiceCRUDImpl implements CustomerServiceCRUD {

    private final CustomerDAO customerDAO;

    public CustomerServiceCRUDImpl(final JdbcRepositoryWrapper jdbc) {
        this.customerDAO = CustomerDAO.getInstance(jdbc);
    }

    @Override
    public Future<Customer> get(final String customerId) {
        return customerDAO.getById(customerId);
    }

    @Override
    public Future<List<Customer>> search(CustomerSearch search) {
        return this.customerDAO.search(search);
    }

    @Override
    public Future<Customer> searchCustomerByRut(Rut rut) {
        return this.customerDAO.searchCustomerByRut(rut);
    }

    @Override
    public Future<Void> saveCustomerFull(Customer customer) {
        return this.customerDAO.saveCustomerFull(customer);
    }

    @Override
    public Future<Void> saveCustomerQuick(Customer customer) {
        return this.customerDAO.saveCustomerQuick(customer);
    }
}
