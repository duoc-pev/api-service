package cl.convenience.store.service.crud.product;

import cl.convenience.store.dao.JdbcRepositoryWrapper;
import cl.convenience.store.dao.ProductsDAO;
import cl.convenience.store.model.product.Brand;
import cl.convenience.store.model.product.Category;
import cl.convenience.store.model.product.Inventory;
import cl.convenience.store.model.product.Product;
import cl.convenience.store.model.product.Unit;
import cl.convenience.store.model.search.ProductSearch;
import io.vertx.core.Future;

import java.util.List;

public class ProductServiceCRUDImpl implements ProductServiceCRUD {

    private final ProductsDAO productsDAO;

    public ProductServiceCRUDImpl(final JdbcRepositoryWrapper jdbc) {
        this.productsDAO = ProductsDAO.getInstance(jdbc);
    }

    @Override
    public Future<List<Category>> getProductCategoryList() {
        return this.productsDAO.getProductCategoryList();
    }

    @Override
    public Future<Category> getProductCategoryById(String categoryId) {
        return this.productsDAO.getProductCategoryById(categoryId);
    }

    @Override
    public Future<List<Unit>> getProductUnitList() {
        return this.productsDAO.getProductUnitList();
    }

    @Override
    public Future<Unit> getProductUnitById(String unitId) {
        return this.productsDAO.getProductUnitById(unitId);
    }

    @Override
    public Future<List<Brand>> getProductBrandList() {
        return this.productsDAO.getProductBrandList();
    }

    @Override
    public Future<Brand> getProductBrandById(String brandId) {
        return this.productsDAO.getProductBrandById(brandId);
    }

    @Override
    public Future<List<Product>> searchProducts(ProductSearch search) {
        return this.productsDAO.searchProductList(search);
    }

    @Override
    public Future<Product> getProductsById(String productId) {
        return this.productsDAO.getProductsById(productId);
    }

    @Override
    public Future<List<Inventory>> getInventoryByProductId(String productId) {
        return this.productsDAO.getInventoryByProductId(productId);
    }
}
