package cl.convenience.store.service.crud.employee;

import cl.convenience.store.dao.EmployeeDAO;
import cl.convenience.store.dao.JdbcRepositoryWrapper;
import cl.convenience.store.model.employee.BranchOffice;
import cl.convenience.store.model.employee.Employee;
import cl.convenience.store.model.employee.Permission;
import cl.convenience.store.model.employee.PermissionGroup;
import cl.convenience.store.model.employee.Role;
import cl.convenience.store.model.search.EmployeeSearch;
import io.vertx.core.Future;

import java.util.List;

public class EmployeeServiceCRUDImpl implements EmployeeServiceCRUD {

    private final EmployeeDAO employeeDAO;

    public EmployeeServiceCRUDImpl(final JdbcRepositoryWrapper jdbc) {
        this.employeeDAO = EmployeeDAO.getInstance(jdbc);
    }

    @Override
    public Future<List<Employee>> searchEmployee(EmployeeSearch search) {
        return this.employeeDAO.searchEmployee(search);
    }

    @Override
    public Future<Employee> getEmployeeById(String employeeId) {
        return this.employeeDAO.getEmployeeById(employeeId);
    }

    @Override
    public Future<List<Role>> getRoleList() {
        return this.employeeDAO.getRoleList();
    }

    @Override
    public Future<List<Permission>> getPermissionByRoleId(String roleId) {
        return this.employeeDAO.getPermissionByRoleId(roleId);
    }

    @Override
    public Future<List<PermissionGroup>> getPermissionGroups() {
        return this.employeeDAO.getPermissionGroups();
    }

    @Override
    public Future<List<BranchOffice>> getBranchList() {
        return this.employeeDAO.getBranchList();
    }

    @Override
    public Future<BranchOffice> getBranchById(String branchId) {
        return this.employeeDAO.getBranchById(branchId);
    }
}
