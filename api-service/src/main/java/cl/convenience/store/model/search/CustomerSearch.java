package cl.convenience.store.model.search;

import cl.convenience.store.model.commons.DateRange;
import cl.convenience.store.model.commons.Pagination;
import cl.convenience.store.model.customer.Customer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerSearch {

    private Customer customer;
    private DateRange creationRange;
    private Pagination pagination;

    public CustomerSearch() {
        this.customer = new Customer();
        this.creationRange = new DateRange();
        this.pagination = new Pagination();
    }
}
