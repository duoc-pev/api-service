package cl.convenience.store.service.business.commons;

import cl.convenience.store.model.commons.address.Address;
import cl.convenience.store.model.commons.person.Phone;
import cl.convenience.store.model.search.AddressSearch;
import cl.convenience.store.model.search.PhoneSearch;
import cl.convenience.store.service.crud.commons.CommonsServiceCRUD;
import io.vertx.core.Future;

import java.util.List;
import java.util.stream.Collectors;

public class CommonsServiceImpl implements CommonsService {

    private final CommonsServiceCRUD commonsServiceCRUD;

    public CommonsServiceImpl(final CommonsServiceCRUD commonsServiceCRUD) {
        this.commonsServiceCRUD = commonsServiceCRUD;
    }

    @Override
    public Future<List<Address>> searchAddress(AddressSearch search) {
        Future<List<Address>> future = Future.future();

        this.commonsServiceCRUD.searchAddress(search).setHandler(addresses -> {
            if (addresses.succeeded()) {
                List<Address> addressList = addresses.result();
                if (null != search.getPagination()) {
                    addressList = addresses.result().stream().skip(search.getPagination().getOffset()).limit(search.getPagination().getLimit()).collect(Collectors.toList());
                }

                future.complete(addressList);
            } else {
                future.fail(addresses.cause());
            }
        });

        return future;
    }

    @Override
    public Future<List<Phone>> searchPhones(PhoneSearch search){
        Future<List<Phone>> future = Future.future();

        this.commonsServiceCRUD.searchPhones(search).setHandler(phones -> {
            if (phones.succeeded()) {
                List<Phone> phoneList = phones.result();
                if (null != search.getPagination()) {
                    phoneList = phones.result().stream().skip(search.getPagination().getOffset()).limit(search.getPagination().getLimit()).collect(Collectors.toList());
                }

                future.complete(phoneList);
            } else {
                future.fail(phones.cause());
            }
        });

        return future;
    }
}
