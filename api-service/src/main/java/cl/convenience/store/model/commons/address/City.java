package cl.convenience.store.model.commons.address;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class City {

    private Integer cityId;
    private String cityName;

    public City(JsonObject json) {
        this.cityId = json.getInteger("CITY_ID");
        this.cityName = json.getString("CITY_NAME");
    }

    public JsonArray convert() {
        JsonArray params = new JsonArray();

        params.add(this.cityId);
        params.add(this.cityName);

        return params;
    }
}
