package cl.convenience.store.verticles.api;

import cl.convenience.store.model.search.ProductSearch;
import cl.convenience.store.service.business.product.ProductService;
import cl.convenience.store.service.crud.product.ProductServiceCRUD;
import cl.convenience.store.verticles.ApiMainVerticle;
import cl.convenience.store.verticles.RestAPIVerticle;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class ProductVerticle extends RestAPIVerticle {

    private static final String API_PRODUCT = ApiMainVerticle.API_CONVENIENCE.concat("/v1/products");
    private static final String API_PROD_CATEGORY = API_PRODUCT.concat("/category");
    private static final String API_PROD_CATEGORY_BY_ID = API_PROD_CATEGORY.concat("/:id");
    private static final String API_PROD_UNIT = API_PRODUCT.concat("/unit");
    private static final String API_PROD_UNIT_BY_ID = API_PROD_UNIT.concat("/:id");
    private static final String API_PROD_BRAND = API_PRODUCT.concat("/brand");
    private static final String API_PROD_BRAND_BY_ID = API_PROD_BRAND.concat("/:id");
    private static final String API_PRODUCTS = API_PRODUCT.concat("/product");
    private static final String API_PRODUCTS_SEARCH = API_PRODUCT.concat("/_search");
    private static final String API_PRODUCTS_BY_ID = API_PRODUCTS.concat("/:id");
    private static final String API_PRODUCT_INVENTORY = API_PRODUCT.concat("/inventory");
    private static final String API_PRODUCT_INVENTORY_BY_ID = API_PRODUCT_INVENTORY.concat("/:id");

    private static final String ID = "id";
    private ProductServiceCRUD productServiceCRUD;
    private ProductService productService;
    
    public ProductVerticle(Router router, ProductServiceCRUD productServiceCRUD, ProductService productService) {
        this.loadRoute(router);
        
        this.productServiceCRUD = productServiceCRUD;
        this.productService = productService;
    }
    
    private void loadRoute(Router router) {
        router.get(API_PROD_CATEGORY).handler(this::apiProductCategoryList);
        router.get(API_PROD_CATEGORY_BY_ID).handler(this::apiProductCategoryById);
        router.get(API_PROD_UNIT).handler(this::apiProductUnitList);
        router.get(API_PROD_UNIT_BY_ID).handler(this::apiProductUnitById);
        router.get(API_PROD_BRAND).handler(this::apiProductBrandList);
        router.get(API_PROD_BRAND_BY_ID).handler(this::apiProductBrandById);
        router.post(API_PRODUCTS_SEARCH).handler(this::apiProductSearch);
        router.get(API_PRODUCTS_BY_ID).handler(this::apiProductsById);
        router.get(API_PRODUCT_INVENTORY_BY_ID).handler(this::apiInventoryByProductId);
    }
    
    private void apiProductCategoryList(RoutingContext context) {
        context.vertx().executeBlocking(future ->
            this.productServiceCRUD.getProductCategoryList().setHandler(resultHandler(context, Json::encodePrettily)), false, resultHandler(context, Json::encodePrettily));
    }
    
    private void apiProductCategoryById(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            
            String categoryId = context.request().getParam(ID);
            this.productServiceCRUD.getProductCategoryById(categoryId).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }

    private void apiProductUnitList(RoutingContext context) {
        context.vertx().executeBlocking(future ->
                this.productServiceCRUD.getProductUnitList().setHandler(resultHandler(context, Json::encodePrettily)), false, resultHandler(context, Json::encodePrettily));
    }

    private void apiProductUnitById(RoutingContext context) {
        context.vertx().executeBlocking(future -> {

            String unitId = context.request().getParam(ID);
            this.productServiceCRUD.getProductUnitById(unitId).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }

    private void apiProductBrandList(RoutingContext context) {
        context.vertx().executeBlocking(future ->
                this.productServiceCRUD.getProductBrandList().setHandler(resultHandler(context, Json::encodePrettily)), false, resultHandler(context, Json::encodePrettily));
    }

    private void apiProductBrandById(RoutingContext context) {
        context.vertx().executeBlocking(future -> {

            String brandId = context.request().getParam(ID);
            this.productServiceCRUD.getProductBrandById(brandId).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }

    private void apiProductSearch(RoutingContext context) {
        context.vertx().executeBlocking(future -> {

            ProductSearch search = Json.decodeValue(context.getBodyAsString(), ProductSearch.class);
            this.productService.searchProducts(search).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }

    private void apiProductsById(RoutingContext context) {
        context.vertx().executeBlocking(future -> {

            String productId = context.request().getParam(ID);
            this.productService.getProductsById(productId).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }

    private void apiInventoryByProductId(RoutingContext context) {
        context.vertx().executeBlocking(future -> {

            String productId = context.request().getParam(ID);
            this.productService.getInventoryByProductId(productId).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }
}
