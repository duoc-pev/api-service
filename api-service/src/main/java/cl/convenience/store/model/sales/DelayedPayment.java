package cl.convenience.store.model.sales;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DelayedPayment {

    private Integer payAmount;
    private Integer orderId;
    private Integer paymentId;
}
