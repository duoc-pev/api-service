package cl.convenience.store.model.search;

import cl.convenience.store.model.commons.Pagination;
import cl.convenience.store.model.employee.BranchOffice;
import lombok.Data;

@Data
public class BranchSearch {

    private BranchOffice branchOffice;
    private Pagination pagination;
}
