package cl.convenience.store.service.business;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

@Slf4j
public class CommonUtil {
    private CommonUtil() {
        throw new IllegalAccessError(CommonUtil.class.toString());
    }

    public static String read(String filename) {
        String text = null;
        try (InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(filename);
             Scanner scanner = new Scanner(stream, "UTF-8")) {
            text = scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return text;
    }



}
