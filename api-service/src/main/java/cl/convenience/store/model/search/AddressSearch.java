package cl.convenience.store.model.search;

import cl.convenience.store.model.commons.Pagination;
import cl.convenience.store.model.commons.address.Address;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddressSearch {

    private Address address;
    private Pagination pagination;

    public AddressSearch(Address address) {
        this.address = address;
    }
}
