package cl.convenience.store.model.sales;

import cl.convenience.store.model.customer.Customer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonArray;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Sale {

    private Customer customer;
    private Integer employeeId;
    private Integer branchId;
    private Integer paymentId;
    private Integer payAmount;
    private List<ProductSale> products;

    public JsonArray convertIn() {
        JsonArray inParams = new JsonArray();

        inParams.add(customer.getCustomerId());
        inParams.add(employeeId);
        inParams.add(paymentId);
        inParams.add(payAmount);

        return inParams;
    }

    public JsonArray convertOut() {
        JsonArray outParams = new JsonArray();

        outParams.addNull().addNull().addNull().addNull().add("INTEGER");

        return outParams;
    }

}
