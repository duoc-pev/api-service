package cl.convenience.store.service.business.customer;

import cl.convenience.store.model.commons.address.Address;
import cl.convenience.store.model.commons.person.Gender;
import cl.convenience.store.model.commons.person.Phone;
import cl.convenience.store.model.customer.Customer;
import cl.convenience.store.service.business.CommonUtil;
import cl.convenience.store.service.crud.commons.CommonsServiceCRUD;
import cl.convenience.store.service.crud.customer.CustomerServiceCRUD;
import cl.convenience.store.util.JsonUtil;
import com.google.common.reflect.TypeToken;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;


@RunWith(PowerMockRunner.class)
public class CustomerServiceImplTest {

    private CustomerService customerService;

    @Mock
    private CustomerServiceCRUD mockCustomerServiceCRUD;

    @Mock
    private CommonsServiceCRUD mockCommonsServiceCRUD;

    private Customer customer;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        this.customerService = new CustomerServiceImpl(mockCustomerServiceCRUD, mockCommonsServiceCRUD);

        this.customer = JsonUtil.GSON.fromJson(CommonUtil.read("customersData.json"), new TypeToken<Customer>(){}.getType());
    }

    @Test
    public void givenCustomer_whenGetCustomerById() {

        Future<Customer> future = Future.future();

        this.futureSearchCustomerResponseNotEmpty(customer);

        customerService.getCustomerById("1").compose(cust -> {
            Future<Address> addressFuture = Future.future();
            Future<Phone> phoneFuture = Future.future();
            Future<Gender> genderFuture = Future.future();

            this.futureAddressNotEmpty(customer).setHandler(addressFuture.completer());
            this.futurePhoneNotEmpty(customer).setHandler(phoneFuture.completer());
            this.futureGenderNotEmpty(customer).setHandler(genderFuture.completer());

            CompositeFuture.all(addressFuture, phoneFuture, genderFuture).compose(event -> {
                assertThat(future.succeeded(), is(Boolean.TRUE));
                assertThat(future.result().getFirstName(), is("Katalin"));
                assertThat(future.result().getEmail(), is("kchester0@behance.net"));

                future.complete(customer);
            }, future);

        }, future);
    }

    @Test
    public void givenCustomer_whenSearhCustomerByRut() {
        Future<Customer> future = Future.future();

        this.futureSearchCustomerResponseNotEmpty(customer);

        customerService.searchCustomerByRut(any()).setHandler(future.completer());

        assertThat(future.succeeded(), is(Boolean.TRUE));
        assertThat(future.result().getRut().getFormattedRut(), is("13.433.441-K"));
    }

    private Future<Customer> futureSearchCustomerResponseNotEmpty(Customer customer) {
        Future<Customer> future = Future.future();
        given(mockCustomerServiceCRUD.get(any())).willReturn(future);
        future.complete(customer);

        return future;
    }

    private Future<Address> futureAddressNotEmpty(Customer customer) {
        Future<Address> future = Future.future();
        given(mockCommonsServiceCRUD.getAddressById(any())).willReturn(future);
        future.complete(customer.getAddress());

        return future;
    }

    private Future<Phone> futurePhoneNotEmpty(Customer customer) {
        Future<Phone> future = Future.future();
        given(mockCommonsServiceCRUD.getPhoneById(any())).willReturn(future);
        future.complete(customer.getPhone());

        return future;
    }

    private Future<Gender> futureGenderNotEmpty(Customer customer) {
        Future<Gender> future = Future.future();
        given(mockCommonsServiceCRUD.getGenderById(any())).willReturn(future);
        future.complete(customer.getGender());

        return future;
    }
}
