package cl.convenience.store.verticles;

import cl.convenience.store.dao.JdbcRepositoryWrapper;
import cl.convenience.store.service.business.commons.CommonsService;
import cl.convenience.store.service.business.commons.CommonsServiceImpl;
import cl.convenience.store.service.business.customer.CustomerService;
import cl.convenience.store.service.business.customer.CustomerServiceImpl;
import cl.convenience.store.service.business.employee.EmployeeService;
import cl.convenience.store.service.business.employee.EmployeeServiceImpl;
import cl.convenience.store.service.business.product.ProductService;
import cl.convenience.store.service.business.product.ProductServiceImpl;
import cl.convenience.store.service.business.sale.SaleService;
import cl.convenience.store.service.business.sale.SaleServiceImpl;
import cl.convenience.store.service.business.supplier.SupplierService;
import cl.convenience.store.service.business.supplier.SupplierServiceImpl;
import cl.convenience.store.service.crud.commons.CommonsServiceCRUD;
import cl.convenience.store.service.crud.commons.CommonsServiceCRUDImpl;
import cl.convenience.store.service.crud.customer.CustomerServiceCRUD;
import cl.convenience.store.service.crud.customer.CustomerServiceCRUDImpl;
import cl.convenience.store.service.crud.employee.EmployeeServiceCRUD;
import cl.convenience.store.service.crud.employee.EmployeeServiceCRUDImpl;
import cl.convenience.store.service.crud.product.ProductServiceCRUD;
import cl.convenience.store.service.crud.product.ProductServiceCRUDImpl;
import cl.convenience.store.service.crud.sale.SaleServiceCRUD;
import cl.convenience.store.service.crud.sale.SaleServiceCRUDImpl;
import cl.convenience.store.service.crud.supplier.SupplierServiceCRUD;
import cl.convenience.store.service.crud.supplier.SupplierServiceCRUDImpl;
import cl.convenience.store.util.AppEnum;
import cl.convenience.store.util.SystemUtil;
import cl.convenience.store.verticles.api.CommonsVerticle;
import cl.convenience.store.verticles.api.CustomerVerticle;
import cl.convenience.store.verticles.api.EmployeeVerticle;
import cl.convenience.store.verticles.api.ProductVerticle;
import cl.convenience.store.verticles.api.SupplierVerticle;
import cl.convenience.store.verticles.api.SalesVerticle;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class ApiMainVerticle extends RestAPIVerticle {

    public static final String API_CONVENIENCE = "/api/convenience";
    private static final String API_HEALTHCHECK = API_CONVENIENCE + "/healthcheck";

    // services
    private CustomerServiceCRUD customerServiceCRUD;
    private CustomerService customerService;
    private CommonsServiceCRUD commonsServiceCRUD;
    private CommonsService commonsService;
    private EmployeeServiceCRUD employeeServiceCRUD;
    private EmployeeService employeeService;
    private ProductServiceCRUD productServiceCRUD;
    private ProductService productService;
    private SupplierServiceCRUD supplierServiceCRUD;
    private SupplierService supplierService;
    private SaleService saleService;
    private SaleServiceCRUD saleServiceCRUD;

    @Override
    public void start() throws Exception {
        super.start();

        // map environment
        // EnvUtil.getInstance();

        // LoginService.getInstance(vertx);

        // principal router
        Router router = Router.router(vertx);

        // body handler
        router.route().handler(BodyHandler.create());

        // services
        final JdbcRepositoryWrapper jdbc = JdbcRepositoryWrapper.getInstance(vertx);

        this.customerServiceCRUD = new CustomerServiceCRUDImpl(jdbc);
        this.commonsServiceCRUD = new CommonsServiceCRUDImpl(jdbc);
        this.employeeServiceCRUD = new EmployeeServiceCRUDImpl(jdbc);
        this.productServiceCRUD = new ProductServiceCRUDImpl(jdbc);
        this.supplierServiceCRUD = new SupplierServiceCRUDImpl(jdbc);
        this.saleServiceCRUD = new SaleServiceCRUDImpl(jdbc);

        this.supplierService = new SupplierServiceImpl(this.supplierServiceCRUD, this.commonsServiceCRUD, this.employeeServiceCRUD, this.productServiceCRUD);
        this.customerService = new CustomerServiceImpl(this.customerServiceCRUD, this.commonsServiceCRUD);
        this.commonsService = new CommonsServiceImpl(this.commonsServiceCRUD);
        this.employeeService = new EmployeeServiceImpl(this.employeeServiceCRUD, this.commonsServiceCRUD);
        this.productService = new ProductServiceImpl(this.productServiceCRUD, this.supplierService, this.employeeServiceCRUD);
        this.saleService = new SaleServiceImpl(this.saleServiceCRUD, this.employeeService, this.employeeServiceCRUD, this.customerServiceCRUD);

        //routes
        this.loadRoute(router);

        //deploy
        this.deployRestVerticle(router);

        //cors support
        this.enableCorsSupport(router);

        //http server
        this.createHttpServer(router, SystemUtil.getEnvironmentIntValue(AppEnum.APP_PORT.name()));
    }

    private void loadRoute(Router router) {
        //healthcheck
        router.get(API_HEALTHCHECK).handler(this::apiHealthCheck);
    }

    private void deployRestVerticle(Router router) {

        // Status API
        vertx.deployVerticle(new CustomerVerticle(router, this.customerService));
        vertx.deployVerticle(new CommonsVerticle(router, this.commonsServiceCRUD, this.commonsService));
        vertx.deployVerticle(new EmployeeVerticle(router, this.employeeServiceCRUD, this.employeeService));
        vertx.deployVerticle(new ProductVerticle(router, this.productServiceCRUD, this.productService));
        vertx.deployVerticle(new SupplierVerticle(router, this.supplierServiceCRUD, this.supplierService));
        vertx.deployVerticle(new SalesVerticle(router, saleService, saleServiceCRUD));
    }

    /**
     * @param context
     */
    private void apiHealthCheck(RoutingContext context) {
        context.response().setStatusCode(200).setStatusMessage("OK").end();
    }
}
