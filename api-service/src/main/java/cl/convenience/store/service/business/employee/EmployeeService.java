package cl.convenience.store.service.business.employee;

import cl.convenience.store.model.employee.Employee;
import cl.convenience.store.model.employee.EmployeeResponse;
import cl.convenience.store.model.search.EmployeeSearch;
import io.vertx.core.Future;

public interface EmployeeService {

    Future<EmployeeResponse> searchEmployees(EmployeeSearch search);

    Future<Employee> getEmployeeById(String employeeId);
}
