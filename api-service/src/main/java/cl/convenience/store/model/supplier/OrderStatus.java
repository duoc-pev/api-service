package cl.convenience.store.model.supplier;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderStatus {

    private Integer id;
    private String name;
    private String description;
    private Integer level;

    public OrderStatus(Integer orderStatusId) {
        this.id = orderStatusId;
    }

    public OrderStatus(JsonObject json) {
        this.id = json.getInteger("SO_STATUS_ID");
        this.name = json.getString("SO_STATUS_NAME");
        this.description = json.getString("SO_STATUS_DESCRIPTION");
        this.level = json.getInteger("SO_STATUS_LEVEL");
    }
}
