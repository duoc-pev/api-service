package cl.convenience.store.service.business.commons;

import cl.convenience.store.model.commons.address.Address;
import cl.convenience.store.model.commons.person.Phone;
import cl.convenience.store.model.search.AddressSearch;
import cl.convenience.store.model.search.PhoneSearch;
import io.vertx.core.Future;

import java.util.List;

public interface CommonsService {

    Future<List<Address>> searchAddress(AddressSearch search);

    Future<List<Phone>> searchPhones(PhoneSearch search);
}
