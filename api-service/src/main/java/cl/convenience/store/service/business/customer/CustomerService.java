package cl.convenience.store.service.business.customer;

import cl.convenience.store.model.commons.person.Rut;
import cl.convenience.store.model.customer.Customer;
import cl.convenience.store.model.customer.CustomerResponse;
import cl.convenience.store.model.search.CustomerSearch;
import io.vertx.core.Future;

public interface CustomerService {

    Future<CustomerResponse> search(CustomerSearch search);

    Future<Customer> getCustomerById(String customerId);

    Future<Customer> searchCustomerByRut(Rut rut);

    Future<Void> saveCustomer(Customer customer);
}
