package cl.convenience.store.verticles.api;

import cl.convenience.store.model.search.OrderSearch;
import cl.convenience.store.model.search.SupplierEmployeeSearch;
import cl.convenience.store.model.search.SupplierSearch;
import cl.convenience.store.service.business.supplier.SupplierService;
import cl.convenience.store.service.crud.supplier.SupplierServiceCRUD;
import cl.convenience.store.verticles.ApiMainVerticle;
import cl.convenience.store.verticles.RestAPIVerticle;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class SupplierVerticle extends RestAPIVerticle {

    private static final String API_SUPPLIERS = ApiMainVerticle.API_CONVENIENCE.concat("/v1/suppliers");
    private static final String API_SUPPLIER_SEARCH = API_SUPPLIERS.concat("/_search");

    private static final String API_SUPPLIER = API_SUPPLIERS.concat("/supplier");
    private static final String API_SUPPLIER_BY_ID = API_SUPPLIER.concat("/:id");

    private static final String API_SUPPLIERS_EMPLOYEES = API_SUPPLIERS.concat("/employees");
    private static final String API_SUPPLIER_EMPLOYEES_SEARCH = API_SUPPLIERS_EMPLOYEES.concat("/_search");
    private static final String API_SUPPLIER_EMP_POSITION = API_SUPPLIERS_EMPLOYEES.concat("/position");
    private static final String API_SUPPLIER_EMPLOYEE = API_SUPPLIERS_EMPLOYEES.concat("/employee");
    private static final String API_SUPPLIER_EMPLOYEE_BY_ID = API_SUPPLIER_EMPLOYEE.concat("1/:id");

    private static final String API_SUPPLIERS_EMPLOYEE_SUP = API_SUPPLIERS_EMPLOYEES.concat("/supplier");
    private static final String API_SUPPLIERS_EMPLOYEE_SUP_ID = API_SUPPLIERS_EMPLOYEE_SUP.concat("/:id");

    private static final String API_SUPPLIER_ORDERS = API_SUPPLIERS.concat("/orders");
    private static final String API_SUPPLIER_ORDERS_SEARCH = API_SUPPLIER_ORDERS.concat("/_search");

    private static final String API_SUPPLIER_ORDER_STATUS_LIST = API_SUPPLIER_ORDERS.concat("/status");
    private static final String API_SUPPLIER_ORDER_STATUS_BY_ID = API_SUPPLIER_ORDER_STATUS_LIST.concat("/:id");

    private static final String API_SUPPLIER_ORDER = API_SUPPLIER_ORDERS.concat("/order");
    private static final String API_SUPPLIER_ORDER_BY_ID = API_SUPPLIER_ORDER.concat("/:id");
    private static final String API_SUPPLIER_ORDER_DETAIL = API_SUPPLIER_ORDER.concat("/detail");
    private static final String API_SUPPLIER_ORDER_DETAIL_BY_ID = API_SUPPLIER_ORDER_DETAIL.concat("/:id");

    private static final String ID = "id";
    private SupplierServiceCRUD supplierServiceCRUD;
    private SupplierService supplierService;

    public SupplierVerticle(Router router, SupplierServiceCRUD supplierServiceCRUD, SupplierService supplierService) {
        this.loadRoute(router);

        this.supplierServiceCRUD = supplierServiceCRUD;
        this.supplierService = supplierService;
    }

    private void loadRoute(Router router) {
        router.post(API_SUPPLIER_SEARCH).handler(this::apiSupplierSearch);
        router.get(API_SUPPLIER_BY_ID).handler(this::apiSupplierById);

        router.post(API_SUPPLIER_EMPLOYEES_SEARCH).handler(this::apiSupplierEmployeeSearch);
        router.get(API_SUPPLIER_EMP_POSITION).handler(this::apiPositionList);
        router.get(API_SUPPLIER_EMPLOYEE_BY_ID).handler(this::apiSupplierEmployeeById);

        router.post(API_SUPPLIERS_EMPLOYEE_SUP_ID).handler(this::apiSupplierEmployeeSearchBySupId);

        router.post(API_SUPPLIER_ORDERS_SEARCH).handler(this::apiOrderSearch);

        router.get(API_SUPPLIER_ORDER_STATUS_LIST).handler(this::apiOrderStatusList);
        router.get(API_SUPPLIER_ORDER_STATUS_BY_ID).handler(this::apiOrderStatusById);

        router.get(API_SUPPLIER_ORDER_BY_ID).handler(this::apiOrderById);
        router.get(API_SUPPLIER_ORDER_DETAIL_BY_ID).handler(this::apiOrderDetailById);
    }

    private void apiSupplierSearch(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            SupplierSearch search = Json.decodeValue(context.getBodyAsString(), SupplierSearch.class);

            this.supplierService.searchSuppliers(search).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }

    private void apiSupplierById(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            String supplierId = context.request().getParam(ID);

            this.supplierService.getSupplierById(supplierId).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }

    private void apiPositionList(RoutingContext context) {
        context.vertx().executeBlocking(future -> this.supplierServiceCRUD.getPositionList().setHandler(resultHandler(context, Json::encodePrettily)), false, resultHandler(context, Json::encodePrettily));
    }

    private void apiSupplierEmployeeSearch(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            SupplierEmployeeSearch search = Json.decodeValue(context.getBodyAsString(), SupplierEmployeeSearch.class);

            this.supplierService.searchSupplierEmployees(search).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }

    private void apiSupplierEmployeeSearchBySupId(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            SupplierEmployeeSearch search = Json.decodeValue(context.getBodyAsString(), SupplierEmployeeSearch.class);

            this.supplierService.searchSupplierEmployeesBySupplierId(search).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }

    private void apiSupplierEmployeeById(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            String employeeId = context.request().getParam(ID);

            this.supplierService.getSupplierEmployeeById(employeeId).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }

    private void apiOrderStatusList(RoutingContext context) {
        context.vertx().executeBlocking(future -> this.supplierServiceCRUD.getOrderStatusList().setHandler(resultHandler(context, Json::encodePrettily)), false, resultHandler(context, Json::encodePrettily));
    }

    private void apiOrderStatusById(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            String orderStatusId = context.request().getParam(ID);

            this.supplierServiceCRUD.getOrderStatusById(orderStatusId).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }

    private void apiOrderSearch(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            OrderSearch search = Json.decodeValue(context.getBodyAsString(), OrderSearch.class);

            this.supplierService.searchOrders(search).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }

    private void apiOrderById(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            String orderId = context.request().getParam(ID);

            this.supplierService.getOrderById(orderId).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }

    private void apiOrderDetailById(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            String orderId = context.request().getParam(ID);

            this.supplierService.getOrderDetailByOrderId(orderId).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }
}
