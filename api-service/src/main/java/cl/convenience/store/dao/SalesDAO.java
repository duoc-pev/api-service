package cl.convenience.store.dao;

import cl.convenience.store.dao.query.QuerySalesUtil;
import cl.convenience.store.exception.AppException;
import cl.convenience.store.model.sales.CustomerOrder;
import cl.convenience.store.model.sales.DelayedPayment;
import cl.convenience.store.model.sales.Payment;
import cl.convenience.store.model.sales.ProductSale;
import cl.convenience.store.model.sales.Sale;
import cl.convenience.store.model.sales.Ticket;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SalesDAO {

    private static SalesDAO instance;
    private final JdbcRepositoryWrapper jdbc;

    private SalesDAO(final JdbcRepositoryWrapper jdbc) {
        this.jdbc = jdbc;
    }

    public static synchronized SalesDAO getInstance(final JdbcRepositoryWrapper jdbc) {
        if (null == instance) {
            instance = new SalesDAO(jdbc);
        }

        return instance;
    }

    public Future<Void> setNewTicket(DelayedPayment delayedPayment) {
        Future<Void> future = Future.future();

        jdbc.executeCall(QuerySalesUtil.saveNewTicketSql(delayedPayment), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete();
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<Integer> createNewSale(Sale sale) {
        Future<Integer> future = Future.future();

        jdbc.executeCallWithParams(QuerySalesUtil.saveNewSaleSql(sale), sale.convertIn(), sale.convertOut(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().getOutput().getInteger(4));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<Ticket> getTicketByOrderId(String orderId) {
        Future<Ticket> future = Future.future();

        jdbc.retrieveOne(Integer.valueOf(orderId), QuerySalesUtil.getTicketByOrderIdSql()).setHandler(optionalAsyncResult -> {
            if (optionalAsyncResult.succeeded()) {
                if (optionalAsyncResult.result().isPresent()) {
                    future.complete(optionalAsyncResult.result().map(Ticket::new).orElse(null));
                } else {
                    future.fail(new AppException("No existe una boleta par el ID de la orden consultada"));
                }
            } else {
                future.fail(optionalAsyncResult.cause());
            }
        });

        return future;
    }

    public Future<Payment> getPaymentById(String paymentId) {
        Future<Payment> future = Future.future();

        jdbc.retrieveOne(paymentId, QuerySalesUtil.getPaymentByIdSql()).setHandler(optionalAsyncResult -> {
            if (optionalAsyncResult.succeeded()) {
                if (optionalAsyncResult.result().isPresent()) {
                    future.complete(optionalAsyncResult.result().map(Payment::new).orElse(null));
                } else {
                    future.fail(new AppException("No existe un medio de pago para el ID enviado"));
                }
            } else {
                future.fail(optionalAsyncResult.cause());
            }
        });

        return future;
    }

    public Future<List<CustomerOrder>> searchCustomerOrders() {
        Future<List<CustomerOrder>> future = Future.future();

        jdbc.retrieveAll(QuerySalesUtil.searchCustomerOrderesSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(CustomerOrder::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<List<Payment>> getPaymentList() {
        Future<List<Payment>> future = Future.future();

        jdbc.retrieveAll(QuerySalesUtil.getPaymentListSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Payment::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }
}
