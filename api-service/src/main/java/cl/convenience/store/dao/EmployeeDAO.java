package cl.convenience.store.dao;

import cl.convenience.store.dao.query.QueryEmployeeUtil;
import cl.convenience.store.exception.AppException;
import cl.convenience.store.model.employee.BranchOffice;
import cl.convenience.store.model.employee.Employee;
import cl.convenience.store.model.employee.Permission;
import cl.convenience.store.model.employee.PermissionGroup;
import cl.convenience.store.model.employee.Role;
import cl.convenience.store.model.search.EmployeeSearch;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static cl.convenience.store.util.MessageUtil.ERROR_BRANCH;
import static cl.convenience.store.util.MessageUtil.ERROR_EMPLOYEE;

public class EmployeeDAO {

    private static EmployeeDAO instance;
    private final JdbcRepositoryWrapper jdbc;

    private EmployeeDAO(final JdbcRepositoryWrapper jdbc) {
        this.jdbc = jdbc;
    }

    public static synchronized EmployeeDAO getInstance(final JdbcRepositoryWrapper jdbc) {
        if (null == instance) {
            instance = new EmployeeDAO(jdbc);
        }
        return instance;
    }

    public Future<List<Employee>> searchEmployee(EmployeeSearch search) {
        Future<List<Employee>> future = Future.future();

        jdbc.retrieveAll(QueryEmployeeUtil.searchEmployeeSql(search), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Employee::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<Employee> getEmployeeById(String employeeId) {
        Future<Employee> future = Future.future();

        jdbc.executeOneResult(new JsonArray().add(employeeId), QueryEmployeeUtil.getEmployeeByIdSql(), optionalAsyncResult -> {
            if (optionalAsyncResult.succeeded()) {
                Optional<JsonObject> jsonObjectOptional = optionalAsyncResult.result();
                if (jsonObjectOptional.isPresent()) {
                    future.complete(new Employee(jsonObjectOptional.get()));
                } else {
                    future.fail(optionalAsyncResult.cause());
                }
            } else {
                future.fail(new AppException(ERROR_EMPLOYEE));
            }
        });

        return future;
    }

    public Future<List<Role>> getRoleList() {
        Future<List<Role>> future = Future.future();

        jdbc.retrieveAll(QueryEmployeeUtil.getRolesListSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Role::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<List<Permission>> getPermissionByRoleId(String roleId) {
        Future<List<Permission>> future = Future.future();

        jdbc.retrieveMany(new JsonArray().add(roleId), QueryEmployeeUtil.getPermissionsByRoleIdSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Permission::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<List<PermissionGroup>> getPermissionGroups() {
        Future<List<PermissionGroup>> future = Future.future();

        jdbc.retrieveAll(QueryEmployeeUtil.getPermissionGroupsSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(PermissionGroup::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<List<BranchOffice>> getBranchList() {
        Future<List<BranchOffice>> future = Future.future();

        jdbc.retrieveAll(QueryEmployeeUtil.getBranchsListSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(BranchOffice::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<BranchOffice> getBranchById(String branchId) {
        Future<BranchOffice> future = Future.future();

        jdbc.executeOneResult(new JsonArray().add(branchId), QueryEmployeeUtil.getBranchByIdSql(), optionalAsyncResult -> {
            if (optionalAsyncResult.succeeded()) {
                if (optionalAsyncResult.result().isPresent()) {
                    future.complete(optionalAsyncResult.result().map(BranchOffice::new).orElse(null));
                } else {
                    future.fail(new AppException(ERROR_BRANCH));
                }
            } else {
                future.fail(optionalAsyncResult.cause());
            }
        });

        return future;
    }
}
