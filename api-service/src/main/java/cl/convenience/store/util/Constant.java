package cl.convenience.store.util;

public class Constant {
    public static final String WITH = "  WITH ";
    public static final String SELECT = "  SELECT ";
    public static final String FROM = " FROM ";
    public static final String WHERE = " WHERE ";
    public static final String UPDATE = " UPDATE ";
    public static final String AND = " AND ";
    public static final String OR = " OR ";
    public static final String SET = "  SET ";
    public static final String ORDER_BY = "  ORDER BY ";
    public static final String LEFT_OUTER_JOIN = " LEFT OUTER JOIN";
    public static final String INNER_JOIN = " INNER JOIN";
    public static final String INSERT = " INSERT";
    public static final String INTO = " INTO";
    public static final String VALUES = " VALUES";

    public static final String ADD = ", ";

    private Constant() {
        throw new IllegalAccessError(Constant.class.toString());
    }
}
