package cl.convenience.store.model.product;

import cl.convenience.store.model.commons.TimeInstant;
import cl.convenience.store.model.employee.BranchOffice;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Inventory {

    private Integer id;
    private Integer productId;
    private BranchOffice branchOffice;
    private Integer quantity;
    private Integer critical;
    private String code;
    private TimeInstant expirationDate;

    public Inventory(JsonObject json) {
        this.id = json.getInteger("BI_ID");
        this.productId = json.getInteger("BI_PRODUCT_ID");
        this.branchOffice = new BranchOffice(json.getInteger("BI_BRANCH_ID"));
        this.quantity = json.getInteger("BI_QUANTITY");
        this.critical = json.getInteger("BI_CRITICAL_Q");
        this.code = json.getString("BI_PRODUCT_CODE");
        this.expirationDate = new TimeInstant(json.getLong("BI_PRODUCT_EXPIRE"));
    }
}
