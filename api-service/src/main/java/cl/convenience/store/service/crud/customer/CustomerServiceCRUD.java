package cl.convenience.store.service.crud.customer;

import cl.convenience.store.model.commons.person.Rut;
import cl.convenience.store.model.customer.Customer;
import cl.convenience.store.model.search.CustomerSearch;
import io.vertx.core.Future;

import java.util.List;

public interface CustomerServiceCRUD {

    Future<Customer> get(final String customerId);

    Future<List<Customer>> search(final CustomerSearch search);

    Future<Customer> searchCustomerByRut(Rut rut);

    Future<Void> saveCustomerFull(Customer customer);

    Future<Void> saveCustomerQuick(Customer customer);
}
