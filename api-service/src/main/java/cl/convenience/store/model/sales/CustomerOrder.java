package cl.convenience.store.model.sales;

import cl.convenience.store.model.commons.TimeInstant;
import cl.convenience.store.model.customer.Customer;
import cl.convenience.store.model.employee.Employee;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerOrder {

    private Integer id;
    private TimeInstant created;
    private TimeInstant payed;
    private Integer totalPrice;
    private Customer customer;
    private Employee employee;
    private Payment payment;

    public CustomerOrder(JsonObject json) {
        this.id = json.getInteger("ORDER_ID");
        this.created = new TimeInstant(json.getLong("ORDER_DATE_PLACED"));
        this.payed = null != json.getLong("ORDER_DATE_PAID") ? new TimeInstant(json.getLong("ORDER_DATE_PAID")) : null;
        this.totalPrice = json.getInteger("ORDER_TOTAL_PRICE");
        this.customer = new Customer(json.getInteger("ORDER_CUSTOMER_ID"));
        this.employee = new Employee(json.getInteger("ORDER_EMP_ID"));
        this.payment = new Payment(json.getInteger("ORDER_PAYMENT_ID"));
    }
}
