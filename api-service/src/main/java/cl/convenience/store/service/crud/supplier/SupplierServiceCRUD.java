package cl.convenience.store.service.crud.supplier;

import cl.convenience.store.model.search.OrderSearch;
import cl.convenience.store.model.search.SupplierEmployeeSearch;
import cl.convenience.store.model.search.SupplierSearch;
import cl.convenience.store.model.supplier.Employee;
import cl.convenience.store.model.supplier.Order;
import cl.convenience.store.model.supplier.OrderDetail;
import cl.convenience.store.model.supplier.OrderStatus;
import cl.convenience.store.model.supplier.Position;
import cl.convenience.store.model.supplier.Supplier;
import io.vertx.core.Future;

import java.util.List;

public interface SupplierServiceCRUD {

    Future<List<Supplier>> searchSupplier(SupplierSearch search);

    Future<Supplier> getSupplierById(String supplierId);

    Future<List<Position>> getPositionList();

    Future<List<Employee>> searchSupplierEmployees(SupplierEmployeeSearch search);

    Future<List<Employee>> getSupplierEmployeesBySupId(String supplierId);

    Future<Employee> getEmployeeById(String employeeId);

    Future<List<OrderStatus>> getOrderStatusList();

    Future<OrderStatus> getOrderStatusById(String orderStatusId);

    Future<List<Order>> searchOrders(OrderSearch search);

    Future<Order> getOrderById(String orderId);

    Future<List<OrderDetail>> getOrderDetailByOrderId(String orderId);
}
