package cl.convenience.store.model.employee;

import cl.convenience.store.model.commons.Count;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeResponse {
    private Count count;
    private List<Employee> employees;
}
