package cl.convenience.store.verticles.api;

import cl.convenience.store.model.commons.Pagination;
import cl.convenience.store.model.sales.DelayedPayment;
import cl.convenience.store.model.sales.Sale;
import cl.convenience.store.model.supplier.Order;
import cl.convenience.store.service.business.sale.SaleService;
import cl.convenience.store.service.crud.sale.SaleServiceCRUD;
import cl.convenience.store.verticles.ApiMainVerticle;
import cl.convenience.store.verticles.RestAPIVerticle;
import cl.convenience.store.verticles.api.util.ValidateSaleUtil;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class SalesVerticle extends RestAPIVerticle {

    private static final String API_SALES = ApiMainVerticle.API_CONVENIENCE.concat("/v1/sales");
    private static final String API_SALE = API_SALES.concat("/sale");
    private static final String API_SALE_SAVE = API_SALE.concat("/_save");
    private static final String API_SALE_SEARCH = API_SALE.concat("/_search");
    private static final String API_SALE_TICKET = API_SALE.concat("/ticket");
    private static final String API_SAVE_TICKET = API_SALE_TICKET.concat("/_save");

    private SaleService saleService;
    private SaleServiceCRUD saleServiceCRUD;

    public SalesVerticle(Router router, SaleService saleService, SaleServiceCRUD saleServiceCRUD) {
        this.loadRoute(router);
        this.saleServiceCRUD = saleServiceCRUD;
        this.saleService = saleService;
    }

    private void loadRoute(Router router) {
        router.post(API_SALE_SAVE).handler(this::apiSaleSave);
        router.post(API_SAVE_TICKET).handler(this::apiSaleSaveTicket);
        router.post(API_SALE_SEARCH).handler(this::apiSaleSearch);
    }

    private void apiSaleSave(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            Sale sale = Json.decodeValue(context.getBodyAsString(), Sale.class);

            ValidateSaleUtil.validate(sale);

            this.saleService.createNewSale(sale).setHandler(resultHandler(context, Json::encodePrettily));

        }, false, resultHandler(context, Json::encodePrettily));
    }

    private void apiSaleSaveTicket(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            DelayedPayment delayedPayment = Json.decodeValue(context.getBodyAsString(), DelayedPayment.class);
            this.saleServiceCRUD.setNewTicket(delayedPayment).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }

    private void apiSaleSearch(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            Pagination pagination = Json.decodeValue(context.getBodyAsString(), Pagination.class);

            this.saleService.searchCustomerOrders(pagination).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }
}
