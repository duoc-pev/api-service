package cl.convenience.store.model.search;

import cl.convenience.store.model.commons.Pagination;
import cl.convenience.store.model.supplier.Supplier;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SupplierSearch {

    private Supplier supplier;
    private Pagination pagination;

    public SupplierSearch() {
        this.supplier = new Supplier();
        this.pagination = null;
    }

    public SupplierSearch (JsonObject json) {
        this.supplier = new Supplier(json);
    }
}
