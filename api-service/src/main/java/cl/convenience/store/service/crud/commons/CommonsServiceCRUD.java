package cl.convenience.store.service.crud.commons;

import cl.convenience.store.model.commons.address.Address;
import cl.convenience.store.model.commons.address.City;
import cl.convenience.store.model.commons.address.Commune;
import cl.convenience.store.model.commons.address.District;
import cl.convenience.store.model.commons.person.Gender;
import cl.convenience.store.model.commons.person.Phone;
import cl.convenience.store.model.commons.person.Rut;
import cl.convenience.store.model.search.AddressSearch;
import cl.convenience.store.model.search.PhoneSearch;
import io.vertx.core.Future;

import java.util.List;

public interface CommonsServiceCRUD {

    Future<Address> getAddressById(String addressId);

    Future<List<Address>> searchAddress(AddressSearch search);

    Future<List<District>> getDistrictList();

    Future<List<City>> getCitiesByDistrictId(String districtId);

    Future<List<Commune>> getCommunesByCityId(String cityId);

    Future<Gender> getGenderById(String genderId);

    Future<List<Gender>> getGenderList();

    Future<Phone> getPhoneById(String phoneId);

    Future<List<Phone>> searchPhones(PhoneSearch search);

    Future<Integer> saveAddress(Address address);

    Future<Integer> savePhone(Phone phone);
}
