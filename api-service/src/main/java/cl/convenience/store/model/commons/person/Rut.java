package cl.convenience.store.model.commons.person;

import cl.convenience.store.util.FormatUtil;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class    Rut {
    private Integer rutPrefix;
    private String dv;
    private String formattedRut;

    public Rut(JsonObject json) {
        this.rutPrefix = json.getInteger("RUT_PREFIX");
        this.dv = json.getString("RUT_DV");
        this.formattedRut = FormatUtil.getFormattedRut(this.rutPrefix, this.dv);
    }

    public JsonArray convert() {
        JsonArray params = new JsonArray();
        params.add(rutPrefix);
        params.add(dv);

        return params;
    }
}
