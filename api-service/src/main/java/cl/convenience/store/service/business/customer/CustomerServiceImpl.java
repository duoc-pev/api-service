package cl.convenience.store.service.business.customer;

import cl.convenience.store.exception.AppException;
import cl.convenience.store.model.commons.Count;
import cl.convenience.store.model.commons.address.Address;
import cl.convenience.store.model.commons.person.Gender;
import cl.convenience.store.model.commons.person.Phone;
import cl.convenience.store.model.commons.person.Rut;
import cl.convenience.store.model.customer.Customer;
import cl.convenience.store.model.customer.CustomerResponse;
import cl.convenience.store.model.search.AddressSearch;
import cl.convenience.store.model.search.CustomerSearch;
import cl.convenience.store.model.search.PhoneSearch;
import cl.convenience.store.service.crud.commons.CommonsServiceCRUD;
import cl.convenience.store.service.crud.customer.CustomerServiceCRUD;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CustomerServiceImpl implements CustomerService {

    private final CustomerServiceCRUD customerServiceCRUD;
    private final CommonsServiceCRUD commonsServiceCRUD;

    public CustomerServiceImpl(final CustomerServiceCRUD customerServiceCRUD, CommonsServiceCRUD commonsServiceCRUD) {
        this.customerServiceCRUD = customerServiceCRUD;
        this.commonsServiceCRUD = commonsServiceCRUD;
    }

    @Override
    public Future<CustomerResponse> search(CustomerSearch search) {
        Future<CustomerResponse> future = Future.future();

        CustomerResponse response = new CustomerResponse();

        this.customerServiceCRUD.search(search).compose(customerList -> {

            Future<List<Customer>> customersWithAddress = Future.future();

            if (CollectionUtils.isNotEmpty(customerList)) {
                AddressSearch addressSearch = new AddressSearch(search.getCustomer().getAddress());

                commonsServiceCRUD.searchAddress(addressSearch).compose(addresses -> {
                    customerList.forEach(customer -> {
                        addressSearch.getAddress().setId(customer.getAddress().getId());
                        customer.setAddress(addresses.stream().filter(c -> c.getId().equals(customer.getAddress().getId())).findFirst().orElse(null));
                    });

                    List<Customer> filteredList = customerList.stream().filter(customer -> Objects.nonNull(customer.getAddress())).collect(Collectors.toList());
                    customersWithAddress.complete(filteredList);

                }, customersWithAddress);
            }

            return customersWithAddress;

        }).compose(customersWithAddress -> {
            Future<List<Customer>> customersWithGender = Future.future();

            if ( CollectionUtils.isNotEmpty(customersWithAddress)) {
                commonsServiceCRUD.getGenderList().compose(genders -> {
                    customersWithAddress.forEach(customer -> customer.setGender(genders.stream().filter(g -> g.getId().equals(customer.getGender().getId())).findFirst().orElse(null)));
                    customersWithGender.complete(customersWithAddress);
                }, customersWithGender);
            }

            return customersWithGender;

        }).compose(customersWithGender -> {
            Future<List<Customer>> customersWithPhones = Future.future();

            if ( CollectionUtils.isNotEmpty(customersWithGender)) {
                PhoneSearch phoneSearch = new PhoneSearch(search.getCustomer().getPhone());

                commonsServiceCRUD.searchPhones(phoneSearch).compose(phones -> {
                    customersWithGender.forEach(customer -> {
                        phoneSearch.getPhone().setPhoneId(customer.getPhone().getPhoneId());
                        customer.setPhone(phones.stream().filter(p -> p.getPhoneId().equals(customer.getPhone().getPhoneId())).findFirst().orElse(null));
                    });
                    List<Customer> filteredList = customersWithGender.stream().filter(customer -> Objects.nonNull(customer.getPhone())).collect(Collectors.toList());
                    customersWithPhones.complete(filteredList);
                }, customersWithPhones);
            }

            return customersWithPhones;
        }).compose(customerListComplete -> {
            response.setCount(new Count(customerListComplete.size()));

            if (null != search.getPagination() && null != search.getPagination().getOffset() && null != search.getPagination().getLimit()) {
                customerListComplete = customerListComplete.stream().skip(search.getPagination().getOffset()).limit(search.getPagination().getLimit()).collect(Collectors.toList());
            }
            response.setCustomers(customerListComplete);

            future.complete(response);
        }, future);

        return future;
    }

    @Override
    public Future<Customer> getCustomerById(String customerId) {
        Future<Customer> future = Future.future();

        this.customerServiceCRUD.get(customerId).compose(customer -> {
            Future<Address> addressFuture = Future.future();
            Future<Phone> phoneFuture = Future.future();
            Future<Gender> genderFuture = Future.future();

            this.commonsServiceCRUD.getAddressById(customer.getAddress().getId().toString()).setHandler(addressFuture.completer());
            this.commonsServiceCRUD.getPhoneById(customer.getPhone().getPhoneId().toString()).setHandler(phoneFuture.completer());
            this.commonsServiceCRUD.getGenderById(customer.getGender().getId().toString()).setHandler(genderFuture.completer());

            CompositeFuture.all(addressFuture, phoneFuture, genderFuture).compose(event -> {
                customer.setAddress(addressFuture.result());
                customer.setPhone(phoneFuture.result());
                customer.setGender(genderFuture.result());

                future.complete(customer);
            }, future);
        }, future);

        return future;
    }

    @Override
    public Future<Customer> searchCustomerByRut(Rut rut) {

        return this.customerServiceCRUD.searchCustomerByRut(rut);
    }

    @Override
    public Future<Void> saveCustomer(Customer customer) {
        if (null != customer.getFirstName()) {
            Future<Void> future = Future.future();

            Future<Integer> addressFuture = Future.future();
            Future<Integer> phoneFuture = Future.future();

            this.commonsServiceCRUD.saveAddress(customer.getAddress()).setHandler(addressFuture.completer());
            this.commonsServiceCRUD.savePhone(customer.getPhone()).setHandler(phoneFuture.completer());

            CompositeFuture.all(addressFuture, phoneFuture).compose(event -> {
                customer.getAddress().setId(addressFuture.result());
                customer.getPhone().setPhoneId(phoneFuture.result());

                this.customerServiceCRUD.saveCustomerFull(customer).compose(cust -> future.complete(), future);
            }, future);

            return future;

        } else if (null != customer.getRut() && null != customer.getEmail()) {
            return this.customerServiceCRUD.saveCustomerQuick(customer);
        } else {
            return Future.failedFuture(new AppException("El usuario no tiene los campos minimos requeridos para ser registrado en el sistema"));
        }
    }
}
