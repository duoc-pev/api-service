package cl.convenience.store.service.business.sale;

import cl.convenience.store.model.commons.Pagination;
import cl.convenience.store.model.sales.CustomerOrder;
import cl.convenience.store.model.sales.CustomerOrderResponse;
import cl.convenience.store.model.sales.Sale;
import cl.convenience.store.model.sales.Ticket;
import io.vertx.core.Future;

import java.util.List;

public interface SaleService {

    Future<Ticket> createNewSale(Sale sale);

    Future<CustomerOrderResponse> searchCustomerOrders(Pagination pagination);
}
