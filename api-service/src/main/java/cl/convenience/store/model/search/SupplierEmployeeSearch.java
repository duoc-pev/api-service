package cl.convenience.store.model.search;

import cl.convenience.store.model.commons.Pagination;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SupplierEmployeeSearch {

    private Integer supplierId;
    private Pagination pagination;
}
