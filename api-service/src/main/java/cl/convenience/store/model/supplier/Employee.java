package cl.convenience.store.model.supplier;

import cl.convenience.store.model.commons.person.Gender;
import cl.convenience.store.model.commons.person.Phone;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Employee {

    private Integer id;
    private String firstName;
    private String lastName;
    private String secondLastName;
    private Position position;
    private Employer employer;
    private Gender gender;
    private String email;
    private Phone phone_1;
    private Phone phone_2;

    public Employee() {
        this.position = new Position();
        this.employer = new Employer();
        this.gender = new Gender();
        this.phone_1 = new Phone();
        this.phone_2 = new Phone();
    }

    public Employee(JsonObject json) {
        this.id = json.getInteger("SE_EMP_ID");
        this.firstName = json.getString("SE_FNAME");
        this.lastName = json.getString("SE_FLASTNAME");
        this.secondLastName = json.getString("SE_SLASTNAME");
        this.position = new Position(json);
        this.employer = new Employer(json);
        this.gender = new Gender(json.getInteger("SE_GENDER_ID"));
        this.email = json.getString("SE_EMAIL");
        this.phone_1 = new Phone(json.getInteger("SE_PHONE_1_ID"));
        this.phone_2 = new Phone(json.getInteger("SE_PHONE_2_ID"));
    }
}
