package cl.convenience.store.model.sales;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProductSold {

    private String sku;
    private String code;
    private String name;
    private Integer quantity;
    private Integer price;
}
