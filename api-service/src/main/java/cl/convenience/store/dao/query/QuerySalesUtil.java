package cl.convenience.store.dao.query;

import cl.convenience.store.model.sales.DelayedPayment;
import cl.convenience.store.model.sales.ProductSale;
import cl.convenience.store.model.sales.Sale;

import java.util.Arrays;

import static cl.convenience.store.util.Constant.FROM;
import static cl.convenience.store.util.Constant.SELECT;
import static cl.convenience.store.util.Constant.WHERE;

public class QuerySalesUtil {

    private QuerySalesUtil() {
        throw new IllegalAccessError(QuerySalesUtil.class.toString());
    }

    public static String saveNewTicketSql(DelayedPayment delayedPayment) {
        StringBuilder query = new StringBuilder();

        query.append("begin ");
        query.append("sp_ticket_generate(");
        query.append(delayedPayment.getOrderId());
        query.append(",");
        query.append(delayedPayment.getPayAmount());
        query.append(",");
        query.append(delayedPayment.getPaymentId());
        query.append("); ");
        query.append("end;");

        return query.toString();
    }

    public static String saveNewSaleSql(Sale sale) {
        Integer[] productsIds = sale.getProducts().stream().map(ProductSale::getId).toArray(Integer[]::new);
        Integer[] productsQuantities = sale.getProducts().stream().map(ProductSale::getQuantity).toArray(Integer[]::new);

        StringBuilder query = new StringBuilder();
        query.append("declare ");
        query.append(" v_products ProductsCodeArrayType := ProductsCodeArrayType(").append(Arrays.toString(productsIds).replace("\"", "").replace("[", "").replace("]", "")).append("); ");
        query.append(" v_quantity ProductsCodeArrayType := ProductsCodeArrayType(").append(Arrays.toString(productsQuantities).replace("\"", "").replace("[", "").replace("]", "")).append("); ");
        query.append("v_order_id Integer; ");
        query.append("begin sp_place_order(?, ?, ?, v_products, v_quantity, ?, ?); end; ");

        return query.toString();
    }

    public static String getTicketByOrderIdSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectTicketOnly());

        return query.toString();
    }

    public static String getPaymentListSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(" pa.payment_id, ");
        query.append(" pa.payment_name, ");
        query.append(" pa.payment_desc ");
        query.append(FROM);
        query.append(" cust_payments pa ");

        return query.toString();
    }

    public static String getPaymentByIdSql() {
        StringBuilder query = new StringBuilder();

        query.append(getPaymentListSql());
        query.append(WHERE);
        query.append(" pa.payment_id = ? ");

        return query.toString();
    }

    private static StringBuilder selectTicketOnly() {
        StringBuilder query = new StringBuilder();

        query.append(" ti.ticket_id, ");
        query.append(" ti.ticket_code, ");
        query.append(" ti.ticket_emitted, ");
        query.append(" round(extract(second from ti.TICKET_EMIT_DATE) + (trunc(ti.TICKET_EMIT_DATE, 'mi') - date '1970-01-01') * (24 * 60 * 60) * 1000) as TICKET_EMIT_DATE, ");
        query.append(" ti.TICKET_TOTAL_AMOUNT, ");
        query.append(" ti.TICKET_NET_VALUE, ");
        query.append(" ti.TICKET_TAX_VALUE, ");
        query.append(" ti.TICKET_PAY_AMOUNT ");
        query.append(FROM);
        query.append(" tickets ti ");
        query.append(WHERE);
        query.append(" ti.ticket_order_id = ?");

        return query;
    }

    public static String searchCustomerOrderesSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(" co.ORDER_ID, ");
        query.append(" round(extract(second from co.ORDER_DATE_PLACED) + (trunc(co.ORDER_DATE_PLACED, 'mi') - date '1970-01-01') * (24 * 60 * 60) * 1000) as ORDER_DATE_PLACED, ");
        query.append(" nvl2(co.ORDER_DATE_PAID, round(extract(second from co.ORDER_DATE_PAID) + (trunc(co.ORDER_DATE_PAID, 'mi') - date '1970-01-01') * (24 * 60 * 60) * 1000), null) as ORDER_DATE_PAID, ");
        query.append(" co.ORDER_TOTAL_PRICE, ");
        query.append(" co.ORDER_CUSTOMER_ID, ");
        query.append(" co.ORDER_EMP_ID, ");
        query.append(" co.ORDER_PAYMENT_ID ");
        query.append(FROM);
        query.append(" cust_orders co ");

        return query.toString();
    }
}
