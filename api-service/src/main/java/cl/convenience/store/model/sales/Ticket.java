package cl.convenience.store.model.sales;

import cl.convenience.store.model.commons.TimeInstant;
import cl.convenience.store.model.employee.Employee;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class Ticket {

    private Integer id;
    private String code;
    private Boolean isEmitted;
    private TimeInstant creationDate;
    private Employee employee;
    private List<ProductSold> products;
    private Integer totalValue;
    private Integer paidValue;
    private Double grossValue;
    private Double taxValue;
    private Integer change;
    private Payment payment;

    public Ticket(JsonObject json) {
        this.id = json.getInteger("TICKET_ID");
        this.code = json.getString("TICKET_CODE");
        this.isEmitted = json.getString("TICKET_EMITTED").equalsIgnoreCase("Y");
        this.creationDate = null != json.getLong("TICKET_EMIT_DATE") ? new TimeInstant(json.getLong("TICKET_EMIT_DATE")) : null;
        this.employee = new Employee();
        this.products = new ArrayList<>();
        this.totalValue = json.getInteger("TICKET_TOTAL_AMOUNT");
        this.grossValue = json.getDouble("TICKET_NET_VALUE");
        this.taxValue = json.getDouble("TICKET_TAX_VALUE");
        this.paidValue = json.getInteger("TICKET_PAY_AMOUNT");
        this.payment = new Payment();
    }
}
