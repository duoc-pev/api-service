package cl.convenience.store.model.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Unit {

    private Integer id;
    private String name;
    private String description;

    public Unit(Integer unitId) {
        this.id = unitId;
    }

    public Unit(JsonObject json) {
        this.id = json.getInteger("PROD_UN_ID");
        this.name = json.getString("PROD_UN_NAME");
        this.description = json.getString("PROD_UN_DESC");
    }
}
