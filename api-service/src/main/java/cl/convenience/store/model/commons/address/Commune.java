package cl.convenience.store.model.commons.address;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Commune {

    private Integer communeId;
    private String communeName;

    public Commune(JsonObject json) {
        this.communeId = json.getInteger("COM_ID");
        this.communeName = json.getString("COM_NAME");
    }

    public JsonArray convert() {
        JsonArray params = new JsonArray();

        params.add(this.communeId);
        params.add(this.communeName);

        return params;
    }
}
