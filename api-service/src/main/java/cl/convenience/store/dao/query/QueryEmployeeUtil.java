package cl.convenience.store.dao.query;

import cl.convenience.store.model.search.EmployeeSearch;

import static cl.convenience.store.util.Constant.ADD;
import static cl.convenience.store.util.Constant.AND;
import static cl.convenience.store.util.Constant.FROM;
import static cl.convenience.store.util.Constant.LEFT_OUTER_JOIN;
import static cl.convenience.store.util.Constant.SELECT;
import static cl.convenience.store.util.Constant.WHERE;

public class QueryEmployeeUtil {

    private QueryEmployeeUtil() {
        throw new IllegalAccessError(QueryEmployeeUtil.class.toString());
    }

    public static String getPermissionsByRoleIdSql() {
        StringBuilder where = new StringBuilder();

        where.append(WHERE);
        where.append(" rp.role_id = ? ");

        return selectRoleAndPermissions().append(where).toString();
    }

    public static String getRolesListSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectRolesOnly());
        query.append(FROM);
        query.append(" roles r ");

        return query.toString();
    }

    public static String getPermissionGroupsSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectPermissionGroupOnly());
        query.append(FROM);
        query.append(" perm_group pg ");

        return query.toString();
    }

    public static String getBranchsListSql() {

        return selectBranchOffices().toString();
    }

    public static String getBranchByIdSql() {
        StringBuilder where = new StringBuilder();

        where.append(WHERE);
        where.append(" bo.branch_id = ? ");

        return selectBranchOffices().append(where).toString();
    }

    public static String searchEmployeeSql(EmployeeSearch search) {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectEmployeeOnly());
        query.append(fromEmployees());
        query.append(searchWhereEmployee(search));

        return query.toString();
    }

    public static String getEmployeeByIdSql() {
        StringBuilder query = new StringBuilder();
        StringBuilder where = new StringBuilder();

        query.append(SELECT);
        query.append(selectEmployeeOnly());

        where.append(WHERE);
        where.append(" e.emp_id = ? ");

        return query.append(fromEmployees()).append(where).toString();

    }
    
    private static StringBuilder selectEmployeeOnly() {
        StringBuilder query = new StringBuilder();
        
        query.append("e.emp_id as employee_id, ");
        query.append("e.emp_fname as first_name, ");
        query.append("e.emp_lname as last_name, ");
        query.append("e.emp_slname as second_last_name, ");
        query.append("e.emp_rut as rut_prefix, ");
        query.append("e.emp_rut_dv as rut_dv, ");
        query.append("e.emp_gender_id as gender_id, ");
        query.append("e.emp_email as email, ");
        query.append("e.emp_phone_id as phone_id, ");
        query.append(" round(extract(second from e.emp_birthday) + (trunc(e.emp_birthday, 'mi') - date '1970-01-01') * (24 * 60 * 60) * 1000) as birthday, ");
        query.append(" extract(year from sysdate) - extract(year from e.emp_birthday) as employee_age, ");
        query.append(" round(extract(second from e.emp_created_at) + (trunc(e.emp_created_at, 'mi') - date '1970-01-01') * (24 * 60 * 60) * 1000) as emp_created_at, ");
        query.append(" round(extract(second from e.emp_hire_date) + (trunc(e.emp_hire_date, 'mi') - date '1970-01-01') * (24 * 60 * 60) * 1000) as emp_hire_date, ");
        query.append(" e.emp_active, ");
        query.append(" e.emp_supervisor_id, ");
        query.append(" e.emp_role_id as role_id, ");
        query.append(" e.emp_add_id as address_id, ");
        query.append(" e.emp_branch_id as branch_id ");

        return query;
    }

    private static StringBuilder searchWhereEmployee(EmployeeSearch search) {
        StringBuilder query = new StringBuilder();

        query.append(WHERE);
        query.append(" e.emp_id > 0 ");

        if (null != search.getEmployee().getId()) {
            query.append(AND);
            query.append(" e.emp_id = ").append(search.getEmployee().getId());
        }

        if (null != search.getEmployee().getFirstName()) {
            query.append(AND);
            query.append(" e.emp_fname LIKE '").append(search.getEmployee().getFirstName()).append("%' ");
        }

        if (null != search.getEmployee().getLastName()) {
            query.append(AND);
            query.append(" e.emp_lname LIKE '").append(search.getEmployee().getLastName()).append("%' ");
        }

        if (null != search.getEmployee().getSecondLastName()){
            query.append(AND);
            query.append(" e.emp_slname LIKE '").append(search.getEmployee().getSecondLastName()).append("%' ");
        }

        if (null != search.getEmployee().getRut().getRutPrefix()) {
            query.append(AND);
            query.append(" e.emp_rut LIKE '").append(search.getEmployee().getRut().getRutPrefix()).append("%' ");
        }

        if (null != search.getEmployee().getEmail()) {
            query.append(AND);
            query.append(" e.emp_email LIKE '").append(search.getEmployee().getEmail()).append("%' ");
        }

        if (null != search.getEmployee().getIsActive()) {
            query.append(AND);
            query.append(" e.emp_active = '").append(search.getEmployee().getIsActive() ? 'Y' : 'N').append("' ");
        }

        if (null != search.getEmployee().getSupervisor()) {
            query.append(AND);
            query.append(" e.emp_supervisor_id = ").append(search.getEmployee().getSupervisor().getId());
        }

        if (null != search.getEmployee().getBranchOffice().getId()) {
            query.append(AND);
            query.append(" e.emp_branch_id = ").append(search.getEmployee().getBranchOffice().getId());
        }

        return query;
    }

    private static StringBuilder selectPermissionOnly() {
        StringBuilder query = new StringBuilder();

        query.append(" p.perm_id, ");
        query.append(" p.perm_pgroup_id, ");
        query.append(" p.perm_active, ");
        query.append(" p.perm_name, ");
        query.append(" p.perm_description ");

        return query;
    }

    private static StringBuilder selectPermissionGroupOnly() {
        StringBuilder query = new StringBuilder();

        query.append(" pg.pgroup_id, ");
        query.append(" pg.pgroup_name, ");
        query.append(" pg.pgroup_desc ");

        return query;
    }

    private static StringBuilder selectRolePermissionOnly() {
        StringBuilder query = new StringBuilder();

        query.append(" rp.role_id, ");
        query.append(" rp.perm_id ");

        return query;
    }

    private static StringBuilder selectRolesOnly() {
        StringBuilder query = new StringBuilder();

        query.append(" r.role_id, ");
        query.append(" r.role_name, ");
        query.append(" r.role_description ");

        return query;
    }

    private static StringBuilder selectBranchOfficeOnly() {
        StringBuilder query = new StringBuilder();

        query.append(" bo.branch_id, ");
        query.append(" bo.branch_add_id as address_id, ");
        query.append(" bo.branch_phone_id as phone_id, ");
        query.append(" round(extract(second from bo.branch_open_date) + (trunc(bo.branch_open_date, 'mi') - date '1970-01-01') * (24 * 60 * 60) * 1000) as branch_open_date, ");
        query.append(" bo.branch_name, ");
        query.append(" bo.branch_active ");

        return query;
    }

    private static StringBuilder selectRoleAndPermissions() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectPermissionOnly());
        query.append(", ");
        query.append(selectRolePermissionOnly());

        query.append(fromWhereRolesAndPermissions());

        return query;
    }

    private static StringBuilder fromWhereRolesAndPermissions() {
        StringBuilder query = new StringBuilder();

        query.append(FROM);
        query.append(" permission p ")
                .append(LEFT_OUTER_JOIN)
                .append(" role_perm rp on (rp.perm_id = p.perm_id) ");

        return query;
    }

    private static StringBuilder fromEmployees() {
        StringBuilder query = new StringBuilder();

        query.append(FROM);
        query.append(" employee e ");

        return query;
    }

    private static StringBuilder selectBranchOffices() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectBranchOfficeOnly());
        query.append(ADD);
        query.append(QueryCommonsUtil.getFullAddressQuery());
        query.append(ADD);
        query.append(QueryCommonsUtil.selectPhoneOnly());
        query.append(FROM);
        query.append(" branch_office bo ");
        query.append(LEFT_OUTER_JOIN);
        query.append(" ADDRESSES ad on (ad.ADD_ID = bo.branch_add_id) ");
        query.append(LEFT_OUTER_JOIN);
        query.append(" PHONE ph on (ph.phone_id = bo.branch_phone_id) ");
        query.append(LEFT_OUTER_JOIN);
        query.append(" COMMUNE com on (com.COM_ID = ad.ADD_COM_ID) ");
        query.append(LEFT_OUTER_JOIN);
        query.append(" CITY ci on (ci.CITY_ID = com.COM_CITY_ID) ");
        query.append(LEFT_OUTER_JOIN);
        query.append(" DISTRICT dist on (dist.DISTRICT_ID = ci.CITY_DISTRICT_ID) ");

        return query;
    }
}
