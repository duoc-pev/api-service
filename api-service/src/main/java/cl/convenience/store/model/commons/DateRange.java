package cl.convenience.store.model.commons;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DateRange {


    private TimeInstant from;
    private TimeInstant to;

    public DateRange(TimeInstant from, TimeInstant to){
        this.from=from;
        this.to=to;
    }

}
