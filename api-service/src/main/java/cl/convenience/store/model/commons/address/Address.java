package cl.convenience.store.model.commons.address;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Address {

    private Integer id;
    private String line1;
    private String line2;
    private Integer postalCode;
    private Commune commune;
    private City city;
    private District district;

    public Address() {
        this.commune = new Commune();
        this.city = new City();
        this.district = new District();
    }

    public Address(JsonObject json) {
        this.id = json.getInteger("ADDRESS_ID");
        this.line1 = json.getString("LINE_1_ADDRESS");
        this.line2 = json.getString("LINE_2_ADDRESS");
        this.postalCode = json.getInteger("POSTAL_CODE");
        this.commune = new Commune(json);
        this.city = new City(json);
        this.district = new District(json);
    }

    public Address(Integer id) {
        this();
        this.id = id;
    }

    public JsonArray inParamsConvert() {
        JsonArray params = new JsonArray();

        params.add(line1);
        if (null != line2) {
            params.add(line2);
        } else {
            params.add("");
        }
        params.add(postalCode);
        params.add(commune.getCommuneId());

        return params;

    }

    public JsonArray outParamsConvert() {
        JsonArray params = new JsonArray();

        params.addNull().addNull().addNull().addNull().add("INTEGER");

        return params;
    }
}
