package cl.convenience.store.verticles.api;

import cl.convenience.store.model.commons.person.Rut;
import cl.convenience.store.model.customer.Customer;
import cl.convenience.store.model.search.CustomerSearch;
import cl.convenience.store.service.business.customer.CustomerService;
import cl.convenience.store.verticles.ApiMainVerticle;
import cl.convenience.store.verticles.RestAPIVerticle;
import cl.convenience.store.verticles.api.util.ValidateApiCustomerUtil;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class CustomerVerticle extends RestAPIVerticle {

    private static final String API_CUSTOMERS = ApiMainVerticle.API_CONVENIENCE.concat("/v1/customer");
    private static final String API_CUSTOMERS_GET = API_CUSTOMERS.concat("/:id");
    private static final String API_CUSTOMERS_SEARCH = API_CUSTOMERS.concat("/_search");
    private static final String API_CUSTOMER_GET_BY_RUT = API_CUSTOMERS.concat("/rut/_search");
    private static final String API_CUSTOMER_SAVE = API_CUSTOMERS.concat("/_save");

    private static final String ID = "id";
    private CustomerService customerService;

    public CustomerVerticle(Router router, CustomerService customerService) {

        // routers
        this.loadRoute(router);

        // services
        this.customerService = customerService;
    }

    public void loadRoute(Router router) {

        router.get(API_CUSTOMERS_GET).handler(this::apiCustomerByID);
        router.post(API_CUSTOMERS_SEARCH).handler(this::apiCustomersSearch);
        router.post(API_CUSTOMER_GET_BY_RUT).handler(this::apiCustomerSearchByRut);
        router.post(API_CUSTOMER_SAVE).handler(this::apiCustomerSave);

    }

    private void apiCustomerByID(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            String customerId = context.request().getParam(ID);
            this.customerService.getCustomerById(customerId).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context));
    }

    private void apiCustomersSearch(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            CustomerSearch search = Json.decodeValue(context.getBodyAsString(), CustomerSearch.class);
            ValidateApiCustomerUtil.search(search);
            this.customerService.search(search).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context));
    }

    private void apiCustomerSearchByRut(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            Rut rut = Json.decodeValue(context.getBodyAsString(), Rut.class);

            this.customerService.searchCustomerByRut(rut).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context));
    }

    private void apiCustomerSave(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            Customer customer = Json.decodeValue(context.getBodyAsString(), Customer.class);

            this.customerService.saveCustomer(customer).setHandler(resultHandler(context));
        }, false, resultHandler(context));
    }
}
