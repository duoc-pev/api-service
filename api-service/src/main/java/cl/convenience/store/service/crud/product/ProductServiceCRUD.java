package cl.convenience.store.service.crud.product;

import cl.convenience.store.model.product.Brand;
import cl.convenience.store.model.product.Category;
import cl.convenience.store.model.product.Inventory;
import cl.convenience.store.model.product.Product;
import cl.convenience.store.model.product.Unit;
import cl.convenience.store.model.search.ProductSearch;
import io.vertx.core.Future;

import java.util.List;

public interface ProductServiceCRUD {

    Future<List<Category>> getProductCategoryList();

    Future<Category> getProductCategoryById(String categoryId);

    Future<List<Unit>> getProductUnitList();

    Future<Unit> getProductUnitById(String unitId);

    Future<List<Brand>> getProductBrandList();

    Future<Brand> getProductBrandById(String brandId);

    Future<List<Product>> searchProducts(ProductSearch search);

    Future<Product> getProductsById(String productId);

    Future<List<Inventory>> getInventoryByProductId(String productId);
}
