package cl.convenience.store.service.business.product;

import cl.convenience.store.model.commons.Count;
import cl.convenience.store.model.employee.BranchOffice;
import cl.convenience.store.model.product.Inventory;
import cl.convenience.store.model.product.Product;
import cl.convenience.store.model.product.ProductResponse;
import cl.convenience.store.model.search.ProductSearch;
import cl.convenience.store.model.search.SupplierSearch;
import cl.convenience.store.model.supplier.SupplierResponse;
import cl.convenience.store.service.business.supplier.SupplierService;
import cl.convenience.store.service.crud.employee.EmployeeServiceCRUD;
import cl.convenience.store.service.crud.product.ProductServiceCRUD;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;

import java.util.List;
import java.util.stream.Collectors;

public class ProductServiceImpl implements ProductService {

    private final ProductServiceCRUD productServiceCRUD;
    private final SupplierService supplierService;
    private final EmployeeServiceCRUD employeeServiceCRUD;

    public ProductServiceImpl(ProductServiceCRUD productServiceCRUD, SupplierService supplierService, EmployeeServiceCRUD employeeServiceCRUD) {
        this.productServiceCRUD = productServiceCRUD;
        this.supplierService = supplierService;
        this.employeeServiceCRUD = employeeServiceCRUD;
    }

    @Override
    public Future<Product> getProductsById(String productId) {
        Future<Product> future = Future.future();

        this.productServiceCRUD.getProductsById(productId).compose(product -> {
            Future<Void> supplierFuture = Future.future();

            this.supplierService.searchSuppliers(new SupplierSearch()).compose(supplierResponse -> {
                product.setSupplier(supplierResponse.getSuppliers().stream().filter(s -> s.getId().equals(product.getSupplier().getId())).findFirst().orElse(null));

                supplierFuture.complete();
                future.complete(product);
            }, future);
        }, future);


        return future;
    }

    @Override
    public Future<ProductResponse> searchProducts(ProductSearch search) {
        Future<ProductResponse> future = Future.future();
        Future<List<Product>> productsFuture = Future.future();

        this.productServiceCRUD.searchProducts(search).compose(products -> {

            this.supplierService.searchSuppliers(new SupplierSearch()).compose(supplierResponse -> {
                products.forEach(p -> p.setSupplier(supplierResponse.getSuppliers().stream().filter(s -> s.getId().equals(p.getSupplier().getId())).findFirst().orElse(null)));

                productsFuture.complete(products);
            }, productsFuture);

            return productsFuture;
        }).compose(products -> {
            ProductResponse response = new ProductResponse();

            response.setCount(new Count(products.size()));

            if (null != search.getPagination()) {
                products = products.stream().skip(search.getPagination().getOffset()).limit(search.getPagination().getLimit()).collect(Collectors.toList());
            }
            response.setProducts(products);

            future.complete(response);
        }, future);

        return future;
    }

    @Override
    public Future<List<Inventory>> getInventoryByProductId(String productId) {
        Future<List<Inventory>> future = Future.future();

        Future<List<Inventory>> futureInventory = Future.future();
        Future<List<BranchOffice>> futureBranch = Future.future();

        this.productServiceCRUD.getInventoryByProductId(productId).setHandler(futureInventory.completer());
        this.employeeServiceCRUD.getBranchList().setHandler(futureBranch.completer());

        CompositeFuture.all(futureBranch, futureInventory).compose(event -> {
            futureInventory.result().forEach(inventory -> inventory.setBranchOffice(futureBranch.result().stream().filter(b -> b.getId().equals(inventory.getBranchOffice().getId())).findFirst().orElse(null)));

            future.complete(futureInventory.result());
        }, future);

        return future;
    }
}
