package cl.convenience.store.verticles.api;

import cl.convenience.store.model.search.AddressSearch;
import cl.convenience.store.model.search.PhoneSearch;
import cl.convenience.store.service.business.commons.CommonsService;
import cl.convenience.store.service.crud.commons.CommonsServiceCRUD;
import cl.convenience.store.verticles.ApiMainVerticle;
import cl.convenience.store.verticles.RestAPIVerticle;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class CommonsVerticle extends RestAPIVerticle {

    private static final String API_COMMONS = ApiMainVerticle.API_CONVENIENCE.concat("/v1/commons");
    private static final String API_COMMONS_GENDER = API_COMMONS.concat("/gender");
    private static final String API_COMMONS_DISTRICT = API_COMMONS.concat("/district");
    private static final String API_COMMONS_CITY = API_COMMONS.concat("/city");
    private static final String API_COMMONS_CITY_GET = API_COMMONS_CITY.concat("/:id");
    private static final String API_COMMONS_COMMUNE = API_COMMONS.concat("/commune");
    private static final String API_COMMONS_COMMUNE_GET = API_COMMONS_COMMUNE.concat("/:id");
    private static final String API_COMMONS_ADDRESS = API_COMMONS.concat("/address");
    private static final String API_COMMONS_ADDRESS_GET = API_COMMONS_ADDRESS.concat("/:id");
    private static final String API_COMMONS_ADDRESS_SEARCH = API_COMMONS_ADDRESS.concat("/_search");
    private static final String API_COMMONS_PHONE = API_COMMONS.concat("/phone");
    private static final String API_COMMONS_PHONE_GET = API_COMMONS_PHONE.concat("/:id");
    private static final String API_COMMONS_PHONE_SEARCH = API_COMMONS_PHONE.concat("/_search");

    private static final String ID = "id";

    private CommonsServiceCRUD commonsServiceCRUD;
    private CommonsService commonsService;

    public CommonsVerticle(Router router, CommonsServiceCRUD commonsServiceCRUD, CommonsService commonsService) {

        // routers
        this.loadRoute(router);

        // services
        this.commonsService = commonsService;
        this.commonsServiceCRUD = commonsServiceCRUD;
    }

    private void loadRoute(Router router) {

        router.get(API_COMMONS_ADDRESS_GET).handler(this::apiCommonsAddressByID);
        router.post(API_COMMONS_ADDRESS_SEARCH).handler(this::apiCommonsAddressSearch);
        router.get(API_COMMONS_DISTRICT).handler(this::apiCommonsGetDistrictList);
        router.get(API_COMMONS_CITY_GET).handler(this::apiCommonsGetCitiesByDistrictId);
        router.get(API_COMMONS_COMMUNE_GET).handler(this::apiCommonsGetCommunesByCityId);
        router.get(API_COMMONS_GENDER).handler(this::apiCommonsGetGenderList);
        router.get(API_COMMONS_PHONE_GET).handler(this::apiCommonsGetPhoneById);
        router.post(API_COMMONS_PHONE_SEARCH).handler(this::apiCommonsSearchPhone);

    }

    private void apiCommonsAddressByID(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            String addressId = context.request().getParam(ID);
            this.commonsServiceCRUD.getAddressById(addressId).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context));
    }

    private void apiCommonsAddressSearch(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            AddressSearch search = Json.decodeValue(context.getBodyAsString(), AddressSearch.class);
            this.commonsService.searchAddress(search).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context));
    }

    private void apiCommonsGetDistrictList(RoutingContext context) {
        context.vertx().executeBlocking(future -> this.commonsServiceCRUD.getDistrictList().setHandler(resultHandler(context, Json::encodePrettily)), false, resultHandler(context));
    }

    private void apiCommonsGetCitiesByDistrictId(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            String districtId = context.request().getParam(ID);
            this.commonsServiceCRUD.getCitiesByDistrictId(districtId).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context));
    }

    private void apiCommonsGetCommunesByCityId(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            String cityId = context.request().getParam(ID);
            this.commonsServiceCRUD.getCommunesByCityId(cityId).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context));
    }

    private void apiCommonsGetGenderList(RoutingContext context) {
        context.vertx().executeBlocking(future -> this.commonsServiceCRUD.getGenderList().setHandler(resultHandler(context, Json::encodePrettily)), false, resultHandler(context));
    }

    private void apiCommonsGetPhoneById(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            String phoneId = context.request().getParam(ID);
            this.commonsServiceCRUD.getPhoneById(phoneId).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context));
    }

    private void apiCommonsSearchPhone(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            PhoneSearch search = Json.decodeValue(context.getBodyAsString(), PhoneSearch.class);
            this.commonsService.searchPhones(search).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context));
    }
}
