package cl.convenience.store.service.business.product;

import cl.convenience.store.model.product.Inventory;
import cl.convenience.store.model.product.Product;
import cl.convenience.store.model.product.ProductResponse;
import cl.convenience.store.model.search.ProductSearch;
import io.vertx.core.Future;

import java.util.List;

public interface ProductService {

    Future<List<Inventory>> getInventoryByProductId(String productId);

    Future<Product> getProductsById(String productId);

    Future<ProductResponse> searchProducts(ProductSearch search);
}
