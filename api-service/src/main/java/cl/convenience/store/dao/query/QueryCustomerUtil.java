package cl.convenience.store.dao.query;

import cl.convenience.store.model.search.CustomerSearch;

import static cl.convenience.store.util.Constant.AND;
import static cl.convenience.store.util.Constant.FROM;
import static cl.convenience.store.util.Constant.INSERT;
import static cl.convenience.store.util.Constant.INTO;
import static cl.convenience.store.util.Constant.SELECT;
import static cl.convenience.store.util.Constant.VALUES;
import static cl.convenience.store.util.Constant.WHERE;

public class QueryCustomerUtil {

    private QueryCustomerUtil() {
        throw new IllegalAccessError(QueryCustomerUtil.class.toString());
    }

    public static String getByIdSql() {
        StringBuilder where = new StringBuilder();
        where
                .append(WHERE)
                .append(" c.cus_id = ? ");
        return select().append(where).toString();
    }

    public static String searchSql(CustomerSearch search) {
        StringBuilder where = new StringBuilder();

        where.append(searchWhere(search));

        return select().append(where).toString();
    }

    public static String searchByRutSql() {
        StringBuilder where = new StringBuilder();

        where.append(WHERE);
        where.append("c.cus_rut = ? and cus_rut_dv = ? ");

        return select().append(where).toString();
    }

    public static String saveCustomerFullSql() {
        StringBuilder query = new StringBuilder();

        query.append(INSERT);
        query.append(INTO);
        query.append(" customers ");
        query.append("(CUS_ID, CUS_FNAME, CUS_FLASTNAME, CUS_SLASTNAME, CUS_RUT, CUS_RUT_DV, CUS_GENDER_ID, CUS_BIRTHDAY, CUS_EMAIL, CUS_ADD_ID, CUS_PHONE_ID) ");
        query.append(VALUES);
        query.append(" (CUSTOMERS_USER_ID_SEQ.nextval, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");

        return query.toString();

    }

    public static String saveCustomerQuickSql() {
        StringBuilder query = new StringBuilder();

        query.append(INSERT);
        query.append(INTO);
        query.append(" customers ");
        query.append("(CUS_RUT, CUS_RUT_DV, CUS_EMAIL) ");
        query.append(VALUES);
        query.append(" (?, ?, ?) ");

        return query.toString();
    }

    private static StringBuilder select() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(" c.cus_id, ");
        query.append(" c.cus_fname as first_name, ");
        query.append(" c.cus_flastname as last_name, ");
        query.append(" c.cus_slastname as second_last_name, ");
        query.append(" c.cus_rut as rut_prefix, ");
        query.append(" c.cus_rut_dv as rut_dv, ");
        query.append(" round(extract(second from c.cus_birthday) + (trunc(c.cus_birthday, 'mi') - date '1970-01-01') * (24 * 60 * 60) * 1000) as birthday, ");
        query.append(" extract(year from sysdate) - extract(year from c.cus_birthday) as customer_age, ");
        query.append(" c.cus_email as email, ");
        query.append(" c.cus_add_id as ADDRESS_ID, ");
        query.append(" c.cus_is_active, ");
        query.append(" round(extract(second from c.cus_created_at) + (trunc(c.cus_created_at, 'mi') - date '1970-01-01') * (24 * 60 * 60) * 1000) as cus_created_at, ");
        query.append(" c.cus_has_credit as has_credit, ");
        query.append(" c.cus_credit_limit as credit_limit, ");
        query.append(" c.cus_credit_pay_day as pay_day, ");
        query.append(" c.cus_gender_id as GENDER_ID, ");
        query.append(" c.cus_phone_id as PHONE_ID ");

        query.append(fromWhere().toString());

        return query;
    }

    private static StringBuilder fromWhere() {
        StringBuilder query = new StringBuilder();

        query.append(FROM);
        query.append(" customers c ");

        return query;
    }

    private static StringBuilder searchWhere(CustomerSearch search) {
        StringBuilder query = new StringBuilder();
        
        query.append(WHERE);
        query.append(" c.cus_id > 0 ");

        if (null != search.getCustomer().getFirstName()) {
            query.append(AND);
            query.append(" c.cus_fname LIKE '").append(search.getCustomer().getFirstName()).append("%' ");
        }

        if (null != search.getCustomer().getLastName()) {
            query.append(AND);
            query.append(" c.cus_flastname LIKE '").append(search.getCustomer().getLastName()).append("%' ");
        }

        if (null != search.getCustomer().getSecondLastName()) {
            query.append(AND);
            query.append(" c.cus_slastname LIKE '").append(search.getCustomer().getSecondLastName()).append("%' ");
        }

        if (null != search.getCustomer().getRut() && null != search.getCustomer().getRut().getRutPrefix()) {
            query.append(AND);
            query.append(" c.cus_rut LIKE '").append(search.getCustomer().getRut().getRutPrefix()).append("%' ");
        }

        if (null != search.getCustomer().getGender() && null != search.getCustomer().getGender().getId()) {
            query.append(AND);
            query.append(" c.cus_gender_id = ").append(search.getCustomer().getGender().getId());
        }

        if (null != search.getCustomer().getEmail()) {
            query.append(AND);
            query.append(" c.cus_email LIKE '").append(search.getCustomer().getEmail()).append("%' ");
        }

        if (null != search.getCustomer().getIsActive()) {
            query.append(AND);
            query.append(" c.cus_is_active = '").append(search.getCustomer().getIsActive() ? 'Y' : 'N').append("' ");
        }

        if (null != search.getCustomer().getCredit() && null != search.getCustomer().getCredit().getHasCredit()) {
            query.append(AND);
            query.append(" c.cus_has_credit = '").append(search.getCustomer().getCredit().getHasCredit() ? 'Y' : 'N').append("' ");
        }

        if (null != search.getCustomer().getCredit() && null != search.getCustomer().getCredit().getCreditLimit()) {
            query.append(AND);
            query.append(" c.cus_credit_limit = '").append(search.getCustomer().getCredit().getCreditLimit()).append("' ");
        }

        if (null != search.getCustomer().getCredit() && null != search.getCustomer().getCredit().getPayDay()) {
            query.append(AND);
            query.append(" c.cus_credit_pay_day = '").append(search.getCustomer().getCredit().getPayDay()).append("' ");
        }

        return query;
    }
}
