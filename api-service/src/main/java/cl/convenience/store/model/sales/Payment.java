package cl.convenience.store.model.sales;

import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Payment {

    private Integer id;
    private String name;
    private String description;

    public Payment(Integer id) {
        this.id = id;
    }

    public Payment(JsonObject json) {
        this.id = json.getInteger("PAYMENT_ID");
        this.name = json.getString("PAYMENT_NAME");
        this.description = json.getString("PAYMENT_DESC");
    }
}
