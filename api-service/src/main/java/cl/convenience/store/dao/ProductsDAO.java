package cl.convenience.store.dao;

import cl.convenience.store.dao.query.QueryProductsUtil;
import cl.convenience.store.exception.AppException;
import cl.convenience.store.model.product.Brand;
import cl.convenience.store.model.product.Category;
import cl.convenience.store.model.product.Inventory;
import cl.convenience.store.model.product.Product;
import cl.convenience.store.model.product.Unit;
import cl.convenience.store.model.search.ProductSearch;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static cl.convenience.store.util.MessageUtil.ERROR_CATEGORY;
import static cl.convenience.store.util.MessageUtil.ERROR_UNIT;

public class ProductsDAO {

    private static ProductsDAO instance;
    private final JdbcRepositoryWrapper jdbc;

    private ProductsDAO(final JdbcRepositoryWrapper jdbc) {
        this.jdbc = jdbc;
    }

    public static synchronized ProductsDAO getInstance(final JdbcRepositoryWrapper jdbc) {
        if (null == instance) {
            instance = new ProductsDAO(jdbc);
        }

        return instance;
    }

    public Future<List<Category>> getProductCategoryList() {
        Future<List<Category>> future = Future.future();

        jdbc.retrieveAll(QueryProductsUtil.getProductCategoryListSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Category::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<Category> getProductCategoryById(String categoryId) {
        Future<Category> future = Future.future();

        jdbc.executeOneResult(new JsonArray().add(categoryId), QueryProductsUtil.getProductCategoryByIdSql(), optionalAsyncResult -> {
            if (optionalAsyncResult.succeeded()) {
                Optional<JsonObject> jsonObjectOptional = optionalAsyncResult.result();

                if (jsonObjectOptional.isPresent()) {
                    future.complete(new Category(jsonObjectOptional.get()));
                } else {
                    future.fail(optionalAsyncResult.cause());
                }
            } else {
                future.fail(new AppException(ERROR_CATEGORY));
            }
        });

        return future;
    }

    public Future<List<Unit>> getProductUnitList() {
        Future<List<Unit>> future = Future.future();

        jdbc.retrieveAll(QueryProductsUtil.getProductUnitListSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Unit::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<Unit> getProductUnitById(String unitId) {
        Future<Unit> future = Future.future();

        jdbc.executeOneResult(new JsonArray().add(unitId), QueryProductsUtil.getProductUnitByIdSql(), optionalAsyncResult -> {
            if (optionalAsyncResult.succeeded()) {
                Optional<JsonObject> jsonObjectOptional = optionalAsyncResult.result();

                if (jsonObjectOptional.isPresent()) {
                    future.complete(new Unit(jsonObjectOptional.get()));
                } else {
                    future.fail(optionalAsyncResult.cause());
                }
            } else {
                future.fail(new AppException(ERROR_UNIT));
            }
        });

        return future;
    }

    public Future<List<Brand>> getProductBrandList() {
        Future<List<Brand>> future = Future.future();

        jdbc.retrieveAll(QueryProductsUtil.getProductBrandListSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Brand::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<Brand> getProductBrandById(String brandId) {
        Future<Brand> future = Future.future();

        jdbc.executeOneResult(new JsonArray().add(brandId), QueryProductsUtil.getProductBrandByIdSql(), optionalAsyncResult -> {
            if (optionalAsyncResult.succeeded()) {
                Optional<JsonObject> jsonObjectOptional = optionalAsyncResult.result();

                if (jsonObjectOptional.isPresent()) {
                    future.complete(new Brand(jsonObjectOptional.get()));
                } else {
                    future.fail(optionalAsyncResult.cause());
                }
            } else {
                future.fail(new AppException(ERROR_UNIT));
            }
        });

        return future;
    }

    public Future<List<Product>> searchProductList(ProductSearch search) {
        Future<List<Product>> future = Future.future();

        jdbc.retrieveAll(QueryProductsUtil.getProductListSql(search), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Product::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<Product> getProductsById(String productId) {
        Future<Product> future = Future.future();

        jdbc.retrieveOne(productId, QueryProductsUtil.getProductByIdSql()).setHandler(optionalAsyncResult -> {
            if (optionalAsyncResult.succeeded()) {
                    future.complete(optionalAsyncResult.result().map(Product::new).orElse(new Product()));
            } else {
                future.fail(optionalAsyncResult.cause());
            }
        });

        return future;
    }

    public Future<List<Inventory>> getInventoryByProductId(String productId) {
        Future<List<Inventory>> future = Future.future();

        jdbc.retrieveMany(new JsonArray().add(productId), QueryProductsUtil.getInventoryByProductIdSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Inventory::new).collect(Collectors.toList()));

            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }
}
