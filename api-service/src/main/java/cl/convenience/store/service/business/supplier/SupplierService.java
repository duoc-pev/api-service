package cl.convenience.store.service.business.supplier;

import cl.convenience.store.model.search.OrderSearch;
import cl.convenience.store.model.search.SupplierEmployeeSearch;
import cl.convenience.store.model.search.SupplierSearch;
import cl.convenience.store.model.supplier.Employee;
import cl.convenience.store.model.supplier.EmployeeResponse;
import cl.convenience.store.model.supplier.Order;
import cl.convenience.store.model.supplier.OrderDetail;
import cl.convenience.store.model.supplier.OrderResponse;
import cl.convenience.store.model.supplier.Supplier;
import cl.convenience.store.model.supplier.SupplierResponse;
import io.vertx.core.Future;

import java.util.List;

public interface SupplierService {

    Future<SupplierResponse> searchSuppliers(SupplierSearch search);

    Future<Supplier> getSupplierById(String supplierId);

    Future<EmployeeResponse> searchSupplierEmployees(SupplierEmployeeSearch search);

    Future<EmployeeResponse> searchSupplierEmployeesBySupplierId(SupplierEmployeeSearch search);

    Future<Employee> getSupplierEmployeeById(String employeeId);

    Future<OrderResponse> searchOrders(OrderSearch search);

    Future<Order> getOrderById(String orderId);

    Future<List<OrderDetail>> getOrderDetailByOrderId(String orderId);
}
