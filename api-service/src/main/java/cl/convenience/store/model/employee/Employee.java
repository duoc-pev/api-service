package cl.convenience.store.model.employee;

import cl.convenience.store.model.commons.TimeInstant;
import cl.convenience.store.model.commons.address.Address;
import cl.convenience.store.model.commons.person.Gender;
import cl.convenience.store.model.commons.person.Phone;
import cl.convenience.store.model.commons.person.Rut;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Employee {

    private Integer id;
    private String firstName;
    private String lastName;
    private String secondLastName;
    private Rut rut;
    private Gender gender;
    private String email;
    private Phone phone;
    private TimeInstant birthday;
    private Integer age;
    private TimeInstant createdAt;
    private TimeInstant hireDate;
    private Boolean isActive;
    private Role role;
    private Address address;
    private Employee supervisor;
    private BranchOffice branchOffice;

    public Employee() {
        this.rut = new Rut();
        this.gender = new Gender();
        this.phone = new Phone();
        this.birthday = new TimeInstant();
        this.createdAt = new TimeInstant();
        this.role = new Role();
        this.address = new Address();
        this.supervisor = null;
        this.branchOffice = new BranchOffice();
    }

    public Employee(Integer employeeId) {
        this();
        this.id = employeeId;
    }

    public Employee(JsonObject json) {
        this();
        this.id = json.getInteger("EMPLOYEE_ID");
        this.firstName = json.getString("FIRST_NAME");
        this.lastName = json.getString("LAST_NAME");
        this.secondLastName = json.getString("SECOND_LAST_NAME");
        this.rut = new Rut(json);
        this.gender = new Gender(json);
        this.email = json.getString("EMAIL");
        this.phone = new Phone(json);
        this.birthday = new TimeInstant(json.getLong("BIRTHDAY"));
        this.age = json.getInteger("EMPLOYEE_AGE");
        this.createdAt = null != json.getLong("EMP_CREATED_AT") ? new TimeInstant(json.getLong("EMP_CREATED_AT")) : null;
        this.hireDate = null != json.getLong("EMP_HIRE_DATE") ? new TimeInstant(json.getLong("EMP_HIRE_DATE")) : null;
        this.isActive = json.getString("EMP_ACTIVE").equalsIgnoreCase("Y");
        this.role = new Role(json);
        this.address = new Address(json);
        if (null != json.getInteger("EMP_SUPERVISOR_ID")) {
            this.supervisor = new Employee(json.getInteger("EMP_SUPERVISOR_ID"));
        }
        this.branchOffice = new BranchOffice(json.getInteger("BRANCH_ID"));
    }
}
