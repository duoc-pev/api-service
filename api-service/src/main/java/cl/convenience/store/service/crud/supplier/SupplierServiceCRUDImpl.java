package cl.convenience.store.service.crud.supplier;

import cl.convenience.store.dao.JdbcRepositoryWrapper;
import cl.convenience.store.dao.SupplierDAO;
import cl.convenience.store.model.employee.BranchOffice;
import cl.convenience.store.model.search.OrderSearch;
import cl.convenience.store.model.search.SupplierEmployeeSearch;
import cl.convenience.store.model.search.SupplierSearch;
import cl.convenience.store.model.supplier.Order;
import cl.convenience.store.model.supplier.OrderDetail;
import cl.convenience.store.model.supplier.OrderStatus;
import cl.convenience.store.model.supplier.Position;
import cl.convenience.store.model.supplier.Supplier;
import cl.convenience.store.model.supplier.Employee;
import io.vertx.core.Future;

import java.util.List;

public class SupplierServiceCRUDImpl implements SupplierServiceCRUD {

    private final SupplierDAO supplierDAO;

    public SupplierServiceCRUDImpl(final JdbcRepositoryWrapper jdbc) {
        this.supplierDAO = SupplierDAO.getInstance(jdbc);
    }

    @Override
    public Future<List<Supplier>> searchSupplier(SupplierSearch search) {
        return this.supplierDAO.searchSupplier(search);
    }

    @Override
    public Future<Supplier> getSupplierById(String supplierId) {
        return this.supplierDAO.getSupplierById(supplierId);
    }

    @Override
    public Future<List<Position>> getPositionList() {
        return this.supplierDAO.getPositionList();
    }

    @Override
    public Future<List<Employee>> searchSupplierEmployees (SupplierEmployeeSearch search) {
        return this.supplierDAO.searchSupplierEmployees(search);
    }

    @Override
    public Future<List<Employee>> getSupplierEmployeesBySupId(String supplierId) {
        return this.supplierDAO.getSupplierEmployeesBySupId(supplierId);
    }

    @Override
    public Future<Employee> getEmployeeById(String employeeId) {
        return this.supplierDAO.getSupplierEmployeeById(employeeId);
    }

    @Override
    public Future<List<OrderStatus>> getOrderStatusList() {
        return this.supplierDAO.getOrderStatusList();
    }

    @Override
    public Future<OrderStatus> getOrderStatusById(String orderStatusId) {
        return this.supplierDAO.getOrderStatusById(orderStatusId);
    }

    @Override
    public Future<List<Order>> searchOrders(OrderSearch search) {
        return this.supplierDAO.searchOrders(search);
    }

    @Override
    public Future<Order> getOrderById(String orderId) {
        return this.supplierDAO.getOrderById(orderId);
    }

    @Override
    public Future<List<OrderDetail>> getOrderDetailByOrderId(String orderId) {
        return this.supplierDAO.getOrderDetailByOrderId(orderId);
    }
}
