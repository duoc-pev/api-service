package cl.convenience.store.model.product;

import cl.convenience.store.model.commons.TimeInstant;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Price {

    private Integer id;
    private Integer productId;
    private Integer supplyOrderId;
    private Integer fullPrice;
    private Double sellNetPrice;
    private Double sellTaxPrice;
    private Double buyNetPrice;
    private Double buyTaxPrice;
    private TimeInstant created;
    private TimeInstant expired;

    public Price(JsonObject json) {
        this.id = json.getInteger("PP_ID");
        this.productId = json.getInteger("PP_PRODUCT_ID");
        this.fullPrice = json.getInteger("PP_FULL_PRICE");
        this.sellNetPrice = json.getDouble("PP_SELL_NET_PRICE");
        this.sellTaxPrice = json.getDouble("PP_SELL_TAX_PRICE");
        this.buyNetPrice = json.getDouble("PP_BUY_NET_PRICE");
        this.buyTaxPrice = json.getDouble("PP_BUY_TAX_PRICE");
        this.created = new TimeInstant(json.getLong("PP_BASE_CREATED"));
        this.expired = new TimeInstant(json.getLong("PP_BASE_EXPIRED"));
    }
}
