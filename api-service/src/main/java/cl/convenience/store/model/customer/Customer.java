package cl.convenience.store.model.customer;

import cl.convenience.store.model.commons.Credit;
import cl.convenience.store.model.commons.person.Gender;
import cl.convenience.store.model.commons.person.Phone;
import cl.convenience.store.model.commons.person.Rut;
import cl.convenience.store.model.commons.TimeInstant;
import cl.convenience.store.model.commons.address.Address;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Customer {

    private Integer customerId;
    private String firstName;
    private String lastName;
    private String secondLastName;
    private Rut rut;
    private Gender gender;
    private TimeInstant birthday;
    private Integer age;
    private String email;
    private Address address;
    private Boolean isActive;
    private TimeInstant createdAt;
    private Credit credit;
    private Phone phone;

    public Customer(Integer id) {
        this.customerId = id;
    }

    public Customer(JsonObject json) {
        this.customerId = json.getInteger("CUS_ID");
        this.firstName = json.getString("FIRST_NAME");
        this.lastName = json.getString("LAST_NAME");
        this.secondLastName = json.getString("SECOND_LAST_NAME");
        this.rut = new Rut(json);
        this.gender = new Gender(json);
        this.birthday = new TimeInstant(Instant.ofEpochMilli(json.getLong("BIRTHDAY")));
        this.age = json.getInteger("CUSTOMER_AGE");
        this.email = json.getString("EMAIL");
        this.address = new Address(json);
        this.isActive = json.getString("CUS_IS_ACTIVE").equalsIgnoreCase("Y");
        this.createdAt = new TimeInstant(Instant.ofEpochMilli(json.getLong("CUS_CREATED_AT")));
        this.credit = new Credit(json);
        this.phone = new Phone(json);
    }

    public JsonArray convertFull() {
        JsonArray params = new JsonArray();

        params.add(firstName);
        params.add(lastName);
        params.add(secondLastName);
        params.add(rut.getRutPrefix());
        params.add(rut.getDv());
        params.add(gender.getId());
        params.add(birthday.getLabel());
        params.add(email);
        params.add(address.getId());
        params.add(phone.getPhoneId());

        return params;
    }

    public JsonArray convertQuick() {
        JsonArray params = new JsonArray();

        params.add(rut.getRutPrefix());
        params.add(rut.getDv());
        params.add(email);

        return params;
    }
}
