package cl.convenience.store.service.crud.sale;

import cl.convenience.store.model.sales.CustomerOrder;
import cl.convenience.store.model.sales.DelayedPayment;
import cl.convenience.store.model.sales.Payment;
import cl.convenience.store.model.sales.Sale;
import cl.convenience.store.model.sales.Ticket;
import cl.convenience.store.model.supplier.Order;
import io.vertx.core.Future;

import java.util.List;

public interface SaleServiceCRUD {

    Future<Void> setNewTicket(DelayedPayment delayedPayment);

    Future<Integer> createNewSale(Sale sale);

    Future<Ticket> getTicketByOrderId(String orderId);

    Future<List<Payment>> getPaymentList();

    Future<Payment> getPaymentById(String paymentId);

    Future<List<CustomerOrder>> searchCustomerOrders();
}
