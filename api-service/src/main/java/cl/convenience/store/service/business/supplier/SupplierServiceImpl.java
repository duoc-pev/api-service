package cl.convenience.store.service.business.supplier;

import cl.convenience.store.model.commons.Count;
import cl.convenience.store.model.commons.address.Address;
import cl.convenience.store.model.commons.person.Gender;
import cl.convenience.store.model.commons.person.Phone;
import cl.convenience.store.model.employee.BranchOffice;
import cl.convenience.store.model.product.Product;
import cl.convenience.store.model.search.AddressSearch;
import cl.convenience.store.model.search.OrderSearch;
import cl.convenience.store.model.search.PhoneSearch;
import cl.convenience.store.model.search.SupplierEmployeeSearch;
import cl.convenience.store.model.search.SupplierSearch;
import cl.convenience.store.model.supplier.Employee;
import cl.convenience.store.model.supplier.EmployeeResponse;
import cl.convenience.store.model.supplier.Order;
import cl.convenience.store.model.supplier.OrderDetail;
import cl.convenience.store.model.supplier.OrderResponse;
import cl.convenience.store.model.supplier.Supplier;
import cl.convenience.store.model.supplier.SupplierResponse;
import cl.convenience.store.service.crud.commons.CommonsServiceCRUD;
import cl.convenience.store.service.crud.employee.EmployeeServiceCRUD;
import cl.convenience.store.service.crud.product.ProductServiceCRUD;
import cl.convenience.store.service.crud.supplier.SupplierServiceCRUD;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class SupplierServiceImpl implements SupplierService {

    private final SupplierServiceCRUD supplierServiceCRUD;
    private final CommonsServiceCRUD commonsServiceCRUD;
    private final EmployeeServiceCRUD employeeServiceCRUD;
    private final ProductServiceCRUD productServiceCRUD;

    public SupplierServiceImpl(SupplierServiceCRUD supplierServiceCRUD, CommonsServiceCRUD commonsServiceCRUD, EmployeeServiceCRUD employeeServiceCRUD, ProductServiceCRUD productServiceCRUD) {
        this.supplierServiceCRUD = supplierServiceCRUD;
        this.commonsServiceCRUD = commonsServiceCRUD;
        this.employeeServiceCRUD = employeeServiceCRUD;
        this.productServiceCRUD = productServiceCRUD;
    }

    @Override
    public Future<SupplierResponse> searchSuppliers(SupplierSearch search) {
        Future<SupplierResponse> future = Future.future();

        SupplierResponse response = new SupplierResponse();

        this.supplierServiceCRUD.searchSupplier(search).compose(suppliers -> {
            Future<List<Supplier>> supplierWithAddresses = Future.future();

            if (CollectionUtils.isNotEmpty(suppliers)) {
                this.commonsServiceCRUD.searchAddress(new AddressSearch(search.getSupplier().getAddress())).compose(addresses -> {
                    suppliers.forEach(supplier -> supplier.setAddress(addresses.stream().filter(a -> a.getId().equals(supplier.getAddress().getId())).findFirst().orElse(null)));

                    List<Supplier> filteredList = suppliers.stream().filter(supplier -> Objects.nonNull(supplier.getAddress())).collect(Collectors.toList());

                    supplierWithAddresses.complete(filteredList);
                }, supplierWithAddresses);
            } else {
                future.complete(new SupplierResponse());
            }

            return supplierWithAddresses;
        }).compose(suppliers -> {
            Future<List<Supplier>> supplierWithPhones = Future.future();

            if (CollectionUtils.isNotEmpty(suppliers)) {
                this.commonsServiceCRUD.searchPhones(new PhoneSearch(search.getSupplier().getPhone())).compose(phones -> {
                    suppliers.forEach(supplier -> supplier.setPhone(phones.stream().filter(p -> p.getPhoneId().equals(supplier.getPhone().getPhoneId())).findFirst().orElse(null)));

                    List<Supplier> filteredList = suppliers.stream().filter(supplier -> Objects.nonNull(supplier.getPhone())).collect(Collectors.toList());

                    supplierWithPhones.complete(filteredList);
                }, supplierWithPhones);
            } else {
                future.complete(new SupplierResponse());
            }

            return supplierWithPhones;
        }).compose(supplierListComplete -> {
            response.setCount(new Count(supplierListComplete.size()));

            if (null != search.getPagination()) {
                supplierListComplete = supplierListComplete.stream().skip(search.getPagination().getOffset()).limit(search.getPagination().getLimit()).collect(Collectors.toList());
            }

            response.setSuppliers(supplierListComplete);

            future.complete(response);
        }, future);

        return future;
    }

    @Override
    public Future<Supplier> getSupplierById(String supplierId) {
        Future<Supplier> future = Future.future();

        this.supplierServiceCRUD.getSupplierById(supplierId).compose(supplier -> {
            Future<Address> addressFuture = Future.future();
            Future<Phone> phoneFuture = Future.future();

            this.commonsServiceCRUD.getAddressById(supplier.getAddress().getId().toString()).setHandler(addressFuture.completer());
            this.commonsServiceCRUD.getPhoneById(supplier.getPhone().getPhoneId().toString()).setHandler(phoneFuture.completer());

            CompositeFuture.all(addressFuture, phoneFuture).compose(result -> {
                supplier.setAddress(addressFuture.result());
                supplier.setPhone(phoneFuture.result());

                future.complete(supplier);
            }, future);
        }, future);

        return future;
    }

    @Override
    public Future<EmployeeResponse> searchSupplierEmployees(SupplierEmployeeSearch search) {
        Future<EmployeeResponse> future = Future.future();
        Future<List<Employee>> employeeListFuture = Future.future();

        this.supplierServiceCRUD.searchSupplierEmployees(search).compose(employees -> {
            Future<List<Phone>> phonesFuture = Future.future();
            Future<List<Gender>> genderFuture = Future.future();

            this.commonsServiceCRUD.searchPhones(new PhoneSearch(new Phone())).setHandler(phonesFuture.completer());
            this.commonsServiceCRUD.getGenderList().setHandler(genderFuture.completer());

            CompositeFuture.all(phonesFuture, genderFuture).compose(event -> {
                employees.forEach(employee -> {
                    employee.setPhone_1(phonesFuture.result().stream().filter(p -> p.getPhoneId().equals(employee.getPhone_1().getPhoneId())).findFirst().orElse(null));
                    employee.setPhone_2(phonesFuture.result().stream().filter(p -> p.getPhoneId().equals(employee.getPhone_2().getPhoneId())).findFirst().orElse(null));
                    employee.setGender(genderFuture.result().stream().filter(g -> g.getId().equals(employee.getGender().getId())).findFirst().orElse(null));
                });

                employeeListFuture.complete(employees);
            }, employeeListFuture);

            return employeeListFuture;
        }).compose(employees -> {
            EmployeeResponse response = new EmployeeResponse();

            response.setCount(new Count(employees.size()));
            response.setEmployees(employees);

            future.complete(response);
        }, future);

        return future;
    }

    @Override
    public Future<EmployeeResponse> searchSupplierEmployeesBySupplierId(SupplierEmployeeSearch search) {
        Future<EmployeeResponse> future = Future.future();
        Future<List<Employee>> employeeList = Future.future();

        this.supplierServiceCRUD.getSupplierEmployeesBySupId(search.getSupplierId().toString()).compose(supplierEmployees -> {
            if (!supplierEmployees.isEmpty()) {
                Future<List<Phone>> phonesFuture = Future.future();
                Future<List<Gender>> genderFuture = Future.future();

                this.commonsServiceCRUD.searchPhones(new PhoneSearch(new Phone())).setHandler(phonesFuture.completer());
                this.commonsServiceCRUD.getGenderList().setHandler(genderFuture.completer());

                CompositeFuture.all(phonesFuture, genderFuture).compose(event -> {
                    supplierEmployees.forEach(employee -> {
                        employee.setPhone_1(phonesFuture.result().stream().filter(p -> p.getPhoneId().equals(employee.getPhone_1().getPhoneId())).findFirst().orElse(null));
                        employee.setPhone_2(phonesFuture.result().stream().filter(p -> p.getPhoneId().equals(employee.getPhone_2().getPhoneId())).findFirst().orElse(null));
                        employee.setGender(genderFuture.result().stream().filter(g -> g.getId().equals(employee.getGender().getId())).findFirst().orElse(null));
                    });

                    employeeList.complete(supplierEmployees);
                }, employeeList);
            }

            return employeeList;
        }).compose(employees -> {
            EmployeeResponse response = new EmployeeResponse();

            response.setCount(new Count(employees.size()));
            response.setEmployees(employees);

            future.complete(response);
        }, future);

        return future;
    }

    @Override
    public Future<Employee> getSupplierEmployeeById(String employeeId) {
        Future<Employee> future = Future.future();

        this.supplierServiceCRUD.getEmployeeById(employeeId).compose(employee -> {
            Future<Phone> phoneFuture1 = Future.future();
            Future<Phone> phoneFuture2 = Future.future();
            Future<Gender> genderFuture = Future.future();

            this.commonsServiceCRUD.getGenderById(employee.getGender().getId().toString()).setHandler(genderFuture.completer());

            this.commonsServiceCRUD.getPhoneById(employee.getPhone_1().getPhoneId().toString()).setHandler(phoneFuture1.completer());

            if (null != employee.getPhone_2().getPhoneId()) {
                this.commonsServiceCRUD.getPhoneById(employee.getPhone_2().getPhoneId().toString()).setHandler(phoneFuture2.completer());
            } else {
                phoneFuture2.complete(employee.getPhone_2());
            }

            Future<CompositeFuture> compositeFuture = Future.future();

            CompositeFuture.all(phoneFuture1, phoneFuture2, genderFuture).setHandler(compositeFuture.completer());

            compositeFuture.compose(event -> {
                employee.setPhone_1(phoneFuture1.result());
                employee.setPhone_2(phoneFuture2.result());
                employee.setGender(genderFuture.result());

                future.complete(employee);
            }, future);
        }, future);

        return future;
    }

    @Override
    public Future<OrderResponse> searchOrders(OrderSearch search) {
        Future<OrderResponse> future = Future.future();
        OrderResponse response = new OrderResponse();

        this.supplierServiceCRUD.searchOrders(search).compose(orders -> {
            Future<List<Order>> orderListFuture = Future.future();
            Future<Void> branchsFuture = Future.future();

            this.employeeServiceCRUD.getBranchList().compose(branchOffice -> {
                orders.forEach(o -> o.setBranchOffice(branchOffice.stream().filter(b -> b.getAddress().getId().equals(o.getBranchOffice().getAddress().getId())).findFirst().orElse(null)));

                branchsFuture.complete();
                orderListFuture.complete(orders);
            }, branchsFuture);

            return orderListFuture;
        }).compose(orders -> {
            response.setCount(new Count(orders.size()));

            if (null != search.getPagination()) {
                orders = orders.stream().skip(search.getPagination().getOffset()).limit(search.getPagination().getLimit()).collect(Collectors.toList());
            }

            response.setOrders(orders);
            future.complete(response);
        }, future);

        return future;
    }

    @Override
    public Future<Order> getOrderById(String orderId) {
        Future<Order> future = Future.future();

        Future<Order> orderFuture = Future.future();
        Future<List<OrderDetail>> listFuture = Future.future();

        this.supplierServiceCRUD.getOrderDetailByOrderId(orderId).setHandler(listFuture.completer());
        this.supplierServiceCRUD.getOrderById(orderId).setHandler(orderFuture.completer());

        CompositeFuture.all(orderFuture, listFuture).compose(event -> {
            Future<Void> branchOffice = Future.future();

            Order order = orderFuture.result();
            order.setOrderDetails(listFuture.result());

            this.employeeServiceCRUD.getBranchById(order.getBranchOffice().getId().toString()).compose(branch -> {
                order.setBranchOffice(branch);
                branchOffice.complete();
                future.complete(order);
            }, future);

        }, future);

        return future;
    }

    @Override
    public Future<List<OrderDetail>> getOrderDetailByOrderId(String orderId) {
        Future<List<OrderDetail>> future = Future.future();

        /*this.supplierServiceCRUD.getOrderDetailByOrderId(orderId).compose(orderDetail -> {
            List<Future> futures = new ArrayList<>();

            orderDetail.forEach(detail -> {
                Future<Product> productFuture = Future.future();

                detail.setProduct(this.productServiceCRUD.getProductsById(detail.getProduct().getId().toString()).setHandler(productFuture.completer()).result());
                futures.add(productFuture);

                CompositeFuture.all(futures).compose(event -> future.complete(orderDetail), future);
            });
        }, future);*/

        return future;
    }
}
