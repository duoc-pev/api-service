package cl.convenience.store.model.supplier;

import cl.convenience.store.model.product.Product;
import cl.convenience.store.model.product.Unit;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderDetail {

    private Integer id;
    private Integer quantity;
    private Product product;
    private Unit unit;
    private Double netPrice;
    private Double taxPrice;
    private Integer fullPrice;

    public OrderDetail(JsonObject json) {
        this.id = json.getInteger("SOD_ID");
        this.quantity = json.getInteger("SOD_QUANTITY");
        this.product = new Product(json.getInteger("PRODUCT_UN_ID"));
        this.unit = new Unit(json.getInteger("PRODUCT_UN_ID"));
        this.netPrice = json.getDouble("SOD_NET_PRICE");
        this.taxPrice = json.getDouble("SOD_TAX_PRICE");
        this.fullPrice = json.getInteger("SOD_FULL_PRICE");
    }
}
