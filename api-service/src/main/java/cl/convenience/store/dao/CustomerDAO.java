package cl.convenience.store.dao;

import cl.convenience.store.dao.query.QueryCustomerUtil;
import cl.convenience.store.dao.query.QuerySalesUtil;
import cl.convenience.store.exception.AppException;
import cl.convenience.store.model.commons.person.Rut;
import cl.convenience.store.model.customer.Customer;
import cl.convenience.store.model.search.CustomerSearch;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static cl.convenience.store.util.MessageUtil.ERROR_CUSTOMER;

public class CustomerDAO {

    private static CustomerDAO instance;
    private final JdbcRepositoryWrapper jdbc;

    private CustomerDAO(final JdbcRepositoryWrapper jdbc) {
        this.jdbc = jdbc;
    }

    public static synchronized CustomerDAO getInstance(final JdbcRepositoryWrapper jdbc) {
        if (instance == null) {
            instance = new CustomerDAO(jdbc);
        }
        return instance;
    }

    public Future<Customer> getById(String customerId) {
        Future<Customer> future = Future.future();

        jdbc.executeOneResult(new JsonArray().add(customerId), QueryCustomerUtil.getByIdSql(), optionalAsyncResult -> {
            if (optionalAsyncResult.succeeded()) {
                Optional<JsonObject> jsonObjectOptional = optionalAsyncResult.result();
                if (jsonObjectOptional.isPresent()) {
                    future.complete(new Customer(jsonObjectOptional.get()));
                } else {
                    future.fail(new AppException(ERROR_CUSTOMER));
                }
            } else {
                future.fail(optionalAsyncResult.cause());
            }
        });

        return future;
    }

    public Future<List<Customer>> search(CustomerSearch search) {
        Future<List<Customer>> future = Future.future();

        jdbc.retrieveAll(QueryCustomerUtil.searchSql(search), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Customer::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<Customer> searchCustomerByRut(Rut rut) {
        Future<Customer> future = Future.future();

        jdbc.executeOneResult(rut.convert(), QueryCustomerUtil.searchByRutSql(), optionalAsyncResult -> {
            if (optionalAsyncResult.succeeded()) {
                if (optionalAsyncResult.result().isPresent()) {
                    future.complete(optionalAsyncResult.result().map(Customer::new).orElse(null));
                } else {
                    future.fail(new AppException("No existe un usuario con el rut consultado"));
                }
            } else {
                future.fail(optionalAsyncResult.cause());
            }
        });

        return future;
    }

    public Future<Void> saveCustomerFull(Customer customer) {
        Future<Void> future = Future.future();

        jdbc.executeNoResult(customer.convertFull(), QueryCustomerUtil.saveCustomerFullSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete();
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<Void> saveCustomerQuick(Customer customer) {
        Future<Void> future = Future.future();

        jdbc.executeNoResult(customer.convertQuick(), QueryCustomerUtil.saveCustomerQuickSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete();
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }
}
