package cl.convenience.store.model.supplier;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Position {

    private Integer id;
    private String name;
    private String description;

    public Position(JsonObject json) {
        this.id = json.getInteger("POS_ID");
        this.name = json.getString("POS_NAME");
        this.description = json.getString("POS_DESCRIPTION");
    }
}
