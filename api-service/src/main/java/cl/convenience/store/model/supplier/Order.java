package cl.convenience.store.model.supplier;

import cl.convenience.store.model.commons.TimeInstant;
import cl.convenience.store.model.employee.BranchOffice;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Order {

    private Integer id;
    private Supplier supplier;
    private OrderStatus orderStatus;
    private BranchOffice branchOffice;
    private TimeInstant creationDate;
    private TimeInstant receivedDate;
    private List<OrderDetail> orderDetails;

    public Order(JsonObject json) {
        this.id = json.getInteger("SO_ID");
        this.supplier = new Supplier(json.getInteger("SUP_ID"));
        this.orderStatus = new OrderStatus(json.getInteger("SO_STATUS_ID"));
        this.branchOffice = new BranchOffice(json.getInteger("SO_BRANCH_ID"));
        this.creationDate = new TimeInstant(json.getLong("SO_PLACED_DATE"));

        if (null != json.getLong("SO_RECEIVED_DATE")) {
            this.receivedDate = new TimeInstant(json.getLong("SO_RECEIVED_DATE"));
        }

        this.orderDetails = new ArrayList<>();
    }
}
