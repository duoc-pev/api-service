package cl.convenience.store.model.search;

import cl.convenience.store.model.commons.Pagination;
import cl.convenience.store.model.commons.person.Phone;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PhoneSearch {

    private Phone phone;
    private Pagination pagination;

    public PhoneSearch(Phone phone) {
        this.phone = phone;
    }
}
