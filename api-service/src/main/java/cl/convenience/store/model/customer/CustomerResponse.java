package cl.convenience.store.model.customer;

import cl.convenience.store.model.commons.Count;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerResponse {
    private Count count;
    private List<Customer> customers;
}
