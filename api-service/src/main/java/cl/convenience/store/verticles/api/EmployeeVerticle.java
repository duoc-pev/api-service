package cl.convenience.store.verticles.api;

import cl.convenience.store.model.search.EmployeeSearch;
import cl.convenience.store.service.business.employee.EmployeeService;
import cl.convenience.store.service.crud.employee.EmployeeServiceCRUD;
import cl.convenience.store.verticles.ApiMainVerticle;
import cl.convenience.store.verticles.RestAPIVerticle;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class EmployeeVerticle extends RestAPIVerticle {

    private static final String API_EMPLOYEE = ApiMainVerticle.API_CONVENIENCE.concat("/v1/employees");
    private static final String API_EMPLOYEE_GET_BY_ID = EmployeeVerticle.API_EMPLOYEE.concat("/employee/:id");
    private static final String API_EMPLOYEE_SEARCH = EmployeeVerticle.API_EMPLOYEE.concat("/employee/_search");
    private static final String API_ROLE_GET = EmployeeVerticle.API_EMPLOYEE.concat("/roles");
    private static final String API_PERMISSIONS_BY_ROLE_ID = EmployeeVerticle.API_ROLE_GET.concat("/:id");
    private static final String API_PERMISSION_GROUPS = EmployeeVerticle.API_EMPLOYEE.concat("/groups");
    private static final String API_BRANCH = EmployeeVerticle.API_EMPLOYEE.concat("/branch");
    private static final String API_BRANCH_BY_ID = EmployeeVerticle.API_BRANCH.concat("/:id");

    private static final String ID = "id";
    private EmployeeServiceCRUD employeeServiceCRUD;
    private EmployeeService employeeService;

    public EmployeeVerticle(Router router, EmployeeServiceCRUD employeeServiceCRUD, EmployeeService employeeService) {
        this.loadRoute(router);

        // services
        this.employeeServiceCRUD = employeeServiceCRUD;
        this.employeeService = employeeService;
    }

    private void loadRoute(Router router) {
        router.post(API_EMPLOYEE_SEARCH).handler(this::apiEmployeeSearch);
        router.get(API_EMPLOYEE_GET_BY_ID).handler(this::apiEmployeeGetById);
        router.get(API_ROLE_GET).handler(this::apiRoleGetRoleList);
        router.get(API_PERMISSIONS_BY_ROLE_ID).handler(this::apiPermissionsByRoleId);
        router.get(API_PERMISSION_GROUPS).handler(this::apiPermissionGroups);
        router.get(API_BRANCH).handler(this::apiBranchList);
        router.get(API_BRANCH_BY_ID).handler(this::apiBranchById);
    }

    private void apiEmployeeSearch(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            EmployeeSearch search = Json.decodeValue(context.getBodyAsString(), EmployeeSearch.class);

            this.employeeService.searchEmployees(search).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }

    private void apiEmployeeGetById(RoutingContext context) {
        context.vertx().executeBlocking(future -> {

            String employeeId = context.request().getParam(ID);
            this.employeeService.getEmployeeById(employeeId).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }

    private void apiRoleGetRoleList(RoutingContext context) {
        context.vertx().executeBlocking(future -> this.employeeServiceCRUD.getRoleList().setHandler(resultHandler(context, Json::encodePrettily)), false, resultHandler(context, Json::encodePrettily));
    }

    private void apiPermissionsByRoleId(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            String roleId = context.request().getParam(ID);
            this.employeeServiceCRUD.getPermissionByRoleId(roleId).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }

    private void apiPermissionGroups(RoutingContext context) {
        context.vertx().executeBlocking(future -> this.employeeServiceCRUD.getPermissionGroups().setHandler(resultHandler(context, Json::encodePrettily)), false, resultHandler(context, Json::encodePrettily));
    }

    private void apiBranchList(RoutingContext context) {
        context.vertx().executeBlocking(future -> this.employeeServiceCRUD.getBranchList().setHandler(resultHandler(context, Json::encodePrettily)), false, resultHandler(context, Json::encodePrettily));
    }

    private void apiBranchById(RoutingContext context) {
        context.vertx().executeBlocking(future -> {
            String branchId = context.request().getParam(ID);
            this.employeeServiceCRUD.getBranchById(branchId).setHandler(resultHandler(context, Json::encodePrettily));
        }, false, resultHandler(context, Json::encodePrettily));
    }
}
