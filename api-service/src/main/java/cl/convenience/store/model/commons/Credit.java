package cl.convenience.store.model.commons;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Credit {
    private Boolean hasCredit;
    private Integer creditLimit;
    private Integer payDay;
    private Integer usedAmount;

    public Credit(JsonObject json) {
        this.hasCredit = json.getString("HAS_CREDIT").equalsIgnoreCase("Y");
        this.creditLimit = json.getInteger("CREDIT_LIMIT");
        this.payDay = json.getInteger("PAY_DAY");
        this.usedAmount = json.getInteger("USED_AMOUNT");
    }

    public JsonArray convert() {
        JsonArray params = new JsonArray();

        params.add(hasCredit ? 'Y' : 'N');
        params.add(creditLimit);
        params.add(payDay);

        return params;
    }
}
