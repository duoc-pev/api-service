package cl.convenience.store.verticles.api.util;

import cl.convenience.store.model.commons.DateRange;
import cl.convenience.store.model.commons.Pagination;
import cl.convenience.store.util.InstantUtil;
import com.google.common.base.Preconditions;

public class ValidateApiUtil {
    private static final String VALIDATE_PAGINATION_OFFSET = "Offset must not be null";
    private static final String VALIDATE_PAGINATION_LIMIT = "Limit must not be null";

    private static final String VALIDATE_RANGE_FROM = "From must not be null";
    private static final String VALIDATE_RANGE_TO = "To must not be null";

    protected ValidateApiUtil(String error) {
        throw new IllegalAccessError(error);
    }

    public static boolean isEmpty(String cs) {
        return cs == null || cs.trim().length() == 0;
    }

    public static void validate(Pagination pagination) {
        if (pagination != null && (pagination.getLimit() != null || pagination.getOffset() != null)) {
            Preconditions.checkArgument(pagination.getOffset() != null, VALIDATE_PAGINATION_OFFSET);
            Preconditions.checkArgument(pagination.getLimit() != null, VALIDATE_PAGINATION_LIMIT);
        }
    }

    public static void validate(DateRange range) {
        if (range != null && (InstantUtil.isNotNullTimeInstant(range.getTo()) || InstantUtil.isNotNullTimeInstant(range.getFrom()))) {

            Preconditions.checkArgument(InstantUtil.isNotNullTimeInstant(range.getTo()), VALIDATE_RANGE_TO);
            Preconditions.checkArgument(InstantUtil.isNotNullTimeInstant(range.getFrom()), VALIDATE_RANGE_FROM);

        }
    }
}
