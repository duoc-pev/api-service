package cl.convenience.store.service.business.sale;

import cl.convenience.store.model.commons.Count;
import cl.convenience.store.model.commons.Pagination;
import cl.convenience.store.model.customer.Customer;
import cl.convenience.store.model.customer.CustomerResponse;
import cl.convenience.store.model.employee.BranchOffice;
import cl.convenience.store.model.employee.Employee;
import cl.convenience.store.model.employee.EmployeeResponse;
import cl.convenience.store.model.sales.CustomerOrder;
import cl.convenience.store.model.sales.CustomerOrderResponse;
import cl.convenience.store.model.sales.DelayedPayment;
import cl.convenience.store.model.sales.Payment;
import cl.convenience.store.model.sales.ProductSold;
import cl.convenience.store.model.sales.Sale;
import cl.convenience.store.model.sales.Ticket;
import cl.convenience.store.model.search.CustomerSearch;
import cl.convenience.store.model.search.EmployeeSearch;
import cl.convenience.store.service.business.employee.EmployeeService;
import cl.convenience.store.service.crud.customer.CustomerServiceCRUD;
import cl.convenience.store.service.crud.employee.EmployeeServiceCRUD;
import cl.convenience.store.service.crud.sale.SaleServiceCRUD;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;

import java.util.List;
import java.util.stream.Collectors;

public class SaleServiceImpl implements SaleService {

    private final SaleServiceCRUD saleServiceCRUD;
    private final EmployeeService employeeService;
    private final EmployeeServiceCRUD employeeServiceCRUD;
    private final CustomerServiceCRUD customerServiceCRUD;

    public SaleServiceImpl(SaleServiceCRUD saleServiceCRUD, EmployeeService employeeService, EmployeeServiceCRUD employeeServiceCRUD, CustomerServiceCRUD customerServiceCRUD) {
        this.saleServiceCRUD = saleServiceCRUD;
        this.employeeService = employeeService;
        this.employeeServiceCRUD = employeeServiceCRUD;
        this.customerServiceCRUD = customerServiceCRUD;
    }

    @Override
    public Future<Ticket> createNewSale(Sale sale) {
        Future<Ticket> future = Future.future();

        if (null != sale.getCustomer().getRut().getRutPrefix()) {

        }

        this.saleServiceCRUD.createNewSale(sale).compose(result -> {
            DelayedPayment delayedPayment = new DelayedPayment();
            delayedPayment.setOrderId(result);
            delayedPayment.setPayAmount(sale.getPayAmount());
            delayedPayment.setPaymentId(sale.getPaymentId());

            this.saleServiceCRUD.setNewTicket(delayedPayment).compose(v -> {
                Future<Employee> employeeFuture = Future.future();
                Future<Payment> paymentFuture = Future.future();
                Future<Ticket> ticketFuture = Future.future();

                this.saleServiceCRUD.getTicketByOrderId(result.toString()).setHandler(ticketFuture.completer());

                this.employeeService.getEmployeeById(sale.getEmployeeId().toString()).setHandler(employeeFuture.completer());
                this.saleServiceCRUD.getPaymentById(sale.getPaymentId().toString()).setHandler(paymentFuture.completer());

                CompositeFuture.all(employeeFuture, paymentFuture, ticketFuture).compose(event -> {
                    ticketFuture.result().setEmployee(employeeFuture.result());
                    ticketFuture.result().setPayment(paymentFuture.result());

                    if (!sale.getPaymentId().equals(4)) {
                        ticketFuture.result().setChange(sale.getPayAmount() - ticketFuture.result().getProducts().stream().mapToInt(ProductSold::getPrice).sum());
                    }

                    future.complete(ticketFuture.result());
                }, future);

            }, future);

        }, future);

        return future;
    }

    @Override
    public Future<CustomerOrderResponse> searchCustomerOrders(Pagination pagination) {
        Future<CustomerOrderResponse> future = Future.future();
        CustomerOrderResponse response = new CustomerOrderResponse();

        this.saleServiceCRUD.searchCustomerOrders().compose(result -> {
            Future<List<Employee>> employeeResponseFuture = Future.future();
            Future<List<Payment>> paymentFuture = Future.future();
            Future<List<Customer>> customerFuture = Future.future();

            this.employeeServiceCRUD.searchEmployee(new EmployeeSearch()).setHandler(employeeResponseFuture.completer());
            this.saleServiceCRUD.getPaymentList().setHandler(paymentFuture.completer());
            this.customerServiceCRUD.search(new CustomerSearch()).setHandler(customerFuture.completer());

            CompositeFuture.all(employeeResponseFuture, paymentFuture, customerFuture).compose(event -> {
                result.forEach(order -> order.setEmployee(employeeResponseFuture.result().stream().filter(e -> e.getId().equals(order.getEmployee().getId())).findFirst().orElse(null)));
                result.forEach(order -> order.setPayment(paymentFuture.result().stream().filter(p -> p.getId().equals(order.getPayment().getId())).findFirst().orElse(null)));

                result.forEach(order -> order.setCustomer(customerFuture.result().stream().filter(c -> c.getCustomerId().equals(order.getCustomer().getCustomerId())).findFirst().orElse(null)));

                response.setCount(new Count(result.size()));
                response.setCustomerOrders(result);

                if (null != pagination.getLimit() && null != pagination.getOffset()) {
                    response.setCustomerOrders(result.stream().skip(pagination.getOffset()).limit(pagination.getLimit()).collect(Collectors.toList()));
                }

                future.complete(response);
            }, future);

        }, future);

        return future;
    }
}
