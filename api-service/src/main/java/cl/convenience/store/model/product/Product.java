package cl.convenience.store.model.product;

import cl.convenience.store.model.supplier.Supplier;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product {

    private Integer id;
    private String sku;
    private String name;
    private String description;
    private Brand brand;
    private Category category;
    private Unit unit;
    private Supplier supplier;
    private Price price;
    private List<Inventory> inventory;

    public Product(Integer productId) {
        this.id = productId;
        this.brand = new Brand();
        this.category = new Category();
        this.unit = new Unit();
        this.supplier = new Supplier();
        this.price = new Price();
    }

    public Product(JsonObject json) {
        this.id = json.getInteger("PRODUCT_ID");
        this.sku = json.getString("PRODUCT_SKU");
        this.name = json.getString("PRODUCT_NAME");
        this.description = json.getString("PRODUCT_DESCRIPTION");
        this.brand = new Brand(json);
        this.category = new Category(json);
        this.unit = new Unit(json);
        this.supplier = new Supplier(json.getInteger("SUP_ID"));
        this.price = new Price(json);
        this.inventory = new ArrayList<>();
    }
}
