package cl.convenience.store.dao;

import cl.convenience.store.dao.query.QueryCommonsUtil;
import cl.convenience.store.exception.AppException;
import cl.convenience.store.model.commons.address.Address;
import cl.convenience.store.model.commons.address.City;
import cl.convenience.store.model.commons.address.Commune;
import cl.convenience.store.model.commons.address.District;
import cl.convenience.store.model.commons.person.Gender;
import cl.convenience.store.model.commons.person.Phone;
import cl.convenience.store.model.search.AddressSearch;
import cl.convenience.store.model.search.PhoneSearch;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static cl.convenience.store.util.MessageUtil.ERROR_ADDRESS;
import static cl.convenience.store.util.MessageUtil.ERROR_GENDER;
import static cl.convenience.store.util.MessageUtil.ERROR_PHONE;

public class CommonsDAO {

    private static CommonsDAO instance;
    private final JdbcRepositoryWrapper jdbc;

    private CommonsDAO(final JdbcRepositoryWrapper jdbc) {
        this.jdbc = jdbc;
    }

    public static synchronized CommonsDAO getInstance(final JdbcRepositoryWrapper jdbc) {
        if (null == instance) {
            instance = new CommonsDAO(jdbc);
        }

        return instance;
    }

    public Future<Address> getAddressByID(String addressId) {
        Future<Address> future = Future.future();

        jdbc.executeOneResult(new JsonArray().add(addressId), QueryCommonsUtil.getAddressByIdSql(), optionalAsyncResult -> {
            if (optionalAsyncResult.succeeded()) {
                Optional<JsonObject> jsonObjectOptional = optionalAsyncResult.result();
                if (jsonObjectOptional.isPresent()) {
                    future.complete(new Address(jsonObjectOptional.get()));
                } else {
                    future.fail(new AppException(ERROR_ADDRESS));
                }
            } else {
                future.fail(optionalAsyncResult.cause());
            }
        });

        return future;
    }

    public Future<List<Address>> addressSearch(AddressSearch search) {
        Future<List<Address>> future = Future.future();

        jdbc.retrieveAll(QueryCommonsUtil.searchAddressSql(search), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Address::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<List<District>> getDistrictList() {
        Future<List<District>> future = Future.future();

        jdbc.retrieveAll(QueryCommonsUtil.getDistrictListSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(District::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<List<City>> getCitiesByDistrictID(String districtId) {
        Future<List<City>> future = Future.future();

        jdbc.retrieveMany(new JsonArray().add(districtId), QueryCommonsUtil.getCityByDistrictIdSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(City::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<List<Commune>> getCommunesByCityID(String cityId) {
        Future<List<Commune>> future = Future.future();

        jdbc.retrieveMany(new JsonArray().add(cityId), QueryCommonsUtil.getCommuneByCityIdSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Commune::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<Gender> getGenderById(String genderId) {
        Future<Gender> future = Future.future();

        jdbc.executeOneResult(new JsonArray().add(genderId), QueryCommonsUtil.getGenderByIdSql(), optionalAsyncResult -> {
            if (optionalAsyncResult.succeeded()) {
                Optional<JsonObject> jsonObjectOptional = optionalAsyncResult.result();

                if (jsonObjectOptional.isPresent()) {
                    future.complete(new Gender(jsonObjectOptional.get()));
                } else {
                    future.fail(new AppException(ERROR_GENDER));
                }
            } else {
                future.fail(optionalAsyncResult.cause());
            }
        });

        return future;
    }

    public Future<List<Gender>> getGenderList() {
        Future<List<Gender>> future = Future.future();

        jdbc.retrieveAll(QueryCommonsUtil.getGenderListSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Gender::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<Phone> getPhoneById(String phoneId) {
        Future<Phone> future = Future.future();

        jdbc.executeOneResult(new JsonArray().add(phoneId), QueryCommonsUtil.getPhoneByIdSql(), optionalAsyncResult -> {
            if (optionalAsyncResult.succeeded()) {
                Optional<JsonObject> jsonObjectOptional = optionalAsyncResult.result();

                if (jsonObjectOptional.isPresent()) {
                    future.complete(new Phone(jsonObjectOptional.get()));
                } else {
                    future.fail(new AppException(ERROR_PHONE));
                }
            } else {
                future.fail(optionalAsyncResult.cause());
            }
        });

        return future;
    }

    public Future<List<Phone>> phoneSearch(PhoneSearch search) {
        Future<List<Phone>> future = Future.future();

        jdbc.retrieveAll(QueryCommonsUtil.searchPhoneSql(search), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Phone::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<Integer> saveAddress(Address address) {
        Future<Integer> future = Future.future();

        jdbc.executeCallWithParams(QueryCommonsUtil.saveAddressSql(), address.inParamsConvert(), address.outParamsConvert() , optionalAsyncResult -> {
            if (optionalAsyncResult.succeeded()) {
                future.complete(optionalAsyncResult.result().getOutput().getInteger(4));
            } else {
                future.fail(optionalAsyncResult.cause());
            }
        } );

        return future;
    }

    public Future<Integer> savePhone(Phone phone) {
        Future<Integer> future = Future.future();

        jdbc.executeCallWithParams(QueryCommonsUtil.savePhoneSql(), phone.inParamsConvert(), phone.outParamsConvert(), optionalAsyncResult -> {
            if (optionalAsyncResult.succeeded()) {
                future.complete(optionalAsyncResult.result().getOutput().getInteger(4));
            } else {
                future.fail(optionalAsyncResult.cause());
            }
        });

        return future;
    }
}
