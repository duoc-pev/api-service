package cl.convenience.store.dao;

import cl.convenience.store.dao.query.QuerySupplierUtil;
import cl.convenience.store.exception.AppException;
import cl.convenience.store.model.search.OrderSearch;
import cl.convenience.store.model.search.SupplierEmployeeSearch;
import cl.convenience.store.model.search.SupplierSearch;
import cl.convenience.store.model.supplier.Employee;
import cl.convenience.store.model.supplier.Order;
import cl.convenience.store.model.supplier.OrderDetail;
import cl.convenience.store.model.supplier.OrderStatus;
import cl.convenience.store.model.supplier.Position;
import cl.convenience.store.model.supplier.Supplier;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;

import java.util.List;
import java.util.stream.Collectors;

import static cl.convenience.store.util.MessageUtil.ERROR_EMPLOYEE;

public class SupplierDAO {

    private static SupplierDAO instance;
    private final JdbcRepositoryWrapper jdbc;

    private SupplierDAO(final JdbcRepositoryWrapper jdbc) {
        this.jdbc = jdbc;
    }

    public static synchronized SupplierDAO getInstance(final JdbcRepositoryWrapper jdbc) {
        if (null == instance) {
            instance = new SupplierDAO(jdbc);
        }

        return instance;
    }

    public Future<List<Supplier>> searchSupplier(SupplierSearch search) {
        Future<List<Supplier>> future = Future.future();

        jdbc.retrieveAll(QuerySupplierUtil.searchSupplierSql(search), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Supplier::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<Supplier> getSupplierById(String supplierId) {
        Future<Supplier> future = Future.future();

        jdbc.executeOneResult(new JsonArray().add(supplierId), QuerySupplierUtil.getSupplierByIdSql(), optionalAsyncResult -> {
            if (optionalAsyncResult.succeeded()) {
                if (optionalAsyncResult.result().isPresent()) {
                    future.complete(new Supplier(optionalAsyncResult.result().get()));
                }
            } else {
                future.fail(optionalAsyncResult.cause());
            }
        });

        return future;
    }

    public Future<List<Position>> getPositionList() {
        Future<List<Position>> future = Future.future();

        jdbc.retrieveAll(QuerySupplierUtil.getPositionListSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Position::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<List<Employee>> searchSupplierEmployees(SupplierEmployeeSearch search) {
        Future<List<Employee>> future = Future.future();

        jdbc.retrieveAll(QuerySupplierUtil.searchSupplierEmployeesSql(search), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Employee::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<List<Employee>> getSupplierEmployeesBySupId(String supplierId) {
        Future<List<Employee>> future = Future.future();

        jdbc.retrieveMany(new JsonArray().add(supplierId), QuerySupplierUtil.getEmployeesBySupplierIdSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Employee::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<Employee> getSupplierEmployeeById(String employeeId) {
        Future<Employee> future = Future.future();

        jdbc.executeOneResult(new JsonArray().add(employeeId), QuerySupplierUtil.getEmployeeByIdSql(), optionAsyncResult -> {
            if (optionAsyncResult.succeeded()) {
                if (optionAsyncResult.result().isPresent()) {
                    future.complete(optionAsyncResult.result().map(Employee::new).orElse(null));
                } else {
                    future.fail(new AppException(ERROR_EMPLOYEE));
                }
            } else {
                future.fail(optionAsyncResult.cause());
            }
        });

        return future;
    }

    public Future<List<OrderStatus>> getOrderStatusList() {
        Future<List<OrderStatus>> future = Future.future();

        jdbc.retrieveAll(QuerySupplierUtil.getOrderStatusSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(OrderStatus::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<OrderStatus> getOrderStatusById(String orderStatusId) {
        Future<OrderStatus> future = Future.future();

        jdbc.retrieveOne(new JsonArray().add(orderStatusId), QuerySupplierUtil.getOrderStatusByIdSql()).setHandler(optionalAsyncResult -> {
            if (optionalAsyncResult.succeeded()) {
                future.complete(optionalAsyncResult.result().map(OrderStatus::new).orElse(new OrderStatus()));
            } else {
                future.fail(optionalAsyncResult.cause());
            }
        });

        return future;
    }

    public Future<List<Order>> searchOrders(OrderSearch search) {
        Future<List<Order>> future = Future.future();

        jdbc.retrieveAll(QuerySupplierUtil.getOrderListSql(search), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(Order::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }

    public Future<Order> getOrderById(String orderId) {
        Future<Order> future = Future.future();

        jdbc.retrieveOne(new JsonArray().add(orderId), QuerySupplierUtil.getOrderByIdSql()).setHandler(optionalAsyncResult -> {
            if (optionalAsyncResult.succeeded()) {
                future.complete(optionalAsyncResult.result().map(Order::new).orElse(new Order()));
            } else {
                future.fail(optionalAsyncResult.cause());
            }
        });

        return future;
    }

    public Future<List<OrderDetail>> getOrderDetailByOrderId(String orderId) {
        Future<List<OrderDetail>> future = Future.future();

        jdbc.retrieveMany(new JsonArray().add(orderId), QuerySupplierUtil.getOrderDetailByOrderIdSql(), asyncResult -> {
            if (asyncResult.succeeded()) {
                future.complete(asyncResult.result().stream().map(OrderDetail::new).collect(Collectors.toList()));
            } else {
                future.fail(asyncResult.cause());
            }
        });

        return future;
    }
}
