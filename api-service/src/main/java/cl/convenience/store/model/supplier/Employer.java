package cl.convenience.store.model.supplier;

import cl.convenience.store.model.commons.person.Rut;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Employer {

    private Integer id;
    private String name;
    private Rut rut;
    private String otherDetails;
    private Integer addressId;
    private Integer phoneId;

    public Employer(JsonObject json) {
        this.id = json.getInteger("SUP_ID");
        this.name = json.getString("SUP_NAME");
        this.rut = new Rut(json);
        this.otherDetails = json.getString("SUP_OTHER_DETAILS");
        this.addressId = json.getInteger("SUP_ADD_ID");
        this.phoneId = json.getInteger("SUP_PHONE_ID");
    }
}
