package cl.convenience.store.model.employee;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Role {

    private Integer roleId;
    private String roleName;
    private String roleDescription;

    public Role(JsonObject json) {
        this.roleId = json.getInteger("ROLE_ID");
        this.roleName = json.getString("ROLE_NAME");
        this.roleDescription = json.getString("ROLE_DESCRIPTION");
    }
}
