package cl.convenience.store.service.crud.sale;

import cl.convenience.store.dao.JdbcRepositoryWrapper;
import cl.convenience.store.dao.SalesDAO;
import cl.convenience.store.model.sales.CustomerOrder;
import cl.convenience.store.model.sales.DelayedPayment;
import cl.convenience.store.model.sales.Payment;
import cl.convenience.store.model.sales.Sale;
import cl.convenience.store.model.sales.Ticket;
import io.vertx.core.Future;

import java.util.List;

public class SaleServiceCRUDImpl implements SaleServiceCRUD {

    private final SalesDAO salesDAO;

    public SaleServiceCRUDImpl(final JdbcRepositoryWrapper jdbc) {
        this.salesDAO = SalesDAO.getInstance(jdbc);
    }

    @Override
    public Future<Void> setNewTicket(DelayedPayment delayedPayment) {
        return this.salesDAO.setNewTicket(delayedPayment);
    }

    @Override
    public Future<Integer> createNewSale(Sale sale) {
        return this.salesDAO.createNewSale(sale);
    }

    @Override
    public Future<List<Payment>> getPaymentList() {
        return this.salesDAO.getPaymentList();
    }

    @Override
    public Future<Ticket> getTicketByOrderId(String orderId) {
        return this.salesDAO.getTicketByOrderId(orderId);
    }

    @Override
    public Future<Payment> getPaymentById(String paymentId) {
        return this.salesDAO.getPaymentById(paymentId);
    }

    @Override
    public Future<List<CustomerOrder>> searchCustomerOrders() {
        return this.salesDAO.searchCustomerOrders();
    }
}
