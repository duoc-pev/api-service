package cl.convenience.store.model.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Brand {

    private Integer id;
    private String shortName;
    private String fullName;

    public Brand(JsonObject json) {
        this.id = json.getInteger("BRAND_ID");
        this.shortName = json.getString("BRAND_SNAME");
        this.fullName = json.getString("BRAND_FNAME");
    }
}
