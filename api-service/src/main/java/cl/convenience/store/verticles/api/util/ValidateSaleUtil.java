package cl.convenience.store.verticles.api.util;

import cl.convenience.store.model.customer.Customer;
import cl.convenience.store.model.product.Product;
import cl.convenience.store.model.sales.ProductSale;
import cl.convenience.store.model.sales.Sale;
import com.google.common.base.Preconditions;

import java.util.List;

public class ValidateSaleUtil {

    private static final String VALIDATE_CUSTOMER_RUT_PREFIX = "El rut cliente debe venir en la petición";
    private static final String VALIDATE_CUSTOMER_RUT_DV = "El digito verificador del rut del cliente debe venir en la petición";
    private static final String VALIDATE_EMPLOYEE_ID = "La firma debe incluir el id del vendedor";
    private static final String VALIDATE_PAYMENT_ID = "La firma debe incluir el id de la forma de pago";
    private static final String VALIDATE_PRODUCTS = "La firma debe incluir el listado de productos";

    protected ValidateSaleUtil(String error) {
        throw new IllegalAccessError(error);
    }

    public static void validate(Sale sale) {
        if (null != sale) {
            validate(sale.getCustomer());
            validate(sale.getProducts());
            validate(sale.getEmployeeId(), VALIDATE_EMPLOYEE_ID);
            validate(sale.getPaymentId(), VALIDATE_PAYMENT_ID);
        }
    }

    private static void validate(Customer customer) {
        if (null != customer) {
            Preconditions.checkNotNull(customer.getRut().getRutPrefix(), VALIDATE_CUSTOMER_RUT_PREFIX);
            Preconditions.checkNotNull(customer.getRut().getDv(), VALIDATE_CUSTOMER_RUT_DV);
        }
    }

    private static void validate(Integer id, String msg) {
        if (null != id) {
            Preconditions.checkNotNull(id, msg);
        }
    }

    private static void validate(List<ProductSale> products) {
        if (null != products) {
            Preconditions.checkArgument(products.size() >= 1, VALIDATE_PRODUCTS);
            Preconditions.checkNotNull(products.get(0).getId(), VALIDATE_PRODUCTS);
        }
    }
}
