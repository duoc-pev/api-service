package cl.convenience.store.model.employee;

import cl.convenience.store.model.commons.TimeInstant;
import cl.convenience.store.model.commons.address.Address;
import cl.convenience.store.model.commons.person.Phone;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;

import java.time.Instant;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BranchOffice {

    private Integer id;
    private Address address;
    private Phone phone;
    private TimeInstant openDate;
    private String name;
    private Boolean isActive;

    public BranchOffice() {
        this.address = new Address();
        this.phone = new Phone();
        this.openDate = new TimeInstant();
        this.isActive = Boolean.FALSE;
    }

    public BranchOffice(Integer branchId) {
        this();
        this.id = branchId;
    }

    public BranchOffice(JsonObject json) {
        this();
        this.id = json.getInteger("BRANCH_ID");
        this.address = new Address(json);
        this.phone = new Phone(json);
        this.openDate = new TimeInstant(Instant.ofEpochMilli(json.getLong("BRANCH_OPEN_DATE")));
        this.name = json.getString("BRANCH_NAME");
        this.isActive = json.getString("BRANCH_ACTIVE").equalsIgnoreCase("Y");
    }
}
