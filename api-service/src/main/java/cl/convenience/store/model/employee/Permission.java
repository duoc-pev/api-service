package cl.convenience.store.model.employee;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Permission {

    private Integer id;
    private String name;
    private String description;
    private Integer permGroupId;
    private Boolean isActive;

    public Permission(JsonObject json) {
        this.id = json.getInteger("PERM_ID");
        this.name = json.getString("PERM_NAME");
        this.description = json.getString("PERM_DESCRIPTION");
        this.permGroupId = json.getInteger("PERM_PGROUP_ID");
        this.isActive = json.getString("PERM_ACTIVE").equalsIgnoreCase("Y");
    }
}
