package cl.convenience.store.model.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Category {

    private Integer id;
    private String name;
    private String description;
    private Integer parentCategoryId;

    public Category(JsonObject json) {
        this.id = json.getInteger("PROD_CAT_ID");
        this.name = json.getString("PROD_CAT_NAME");
        this.description = json.getString("PROD_CAT_DESC");
        this.parentCategoryId = json.getInteger("PROD_CAT_PARENT_ID");
    }
}
