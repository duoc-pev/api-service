package cl.convenience.store.model.commons.address;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class District {
    private Integer id;
    private String label;
    private String roman;
    private Integer order;

    public District(JsonObject json) {
        this.id = json.getInteger("DISTRICT_ID");
        this.label = json.getString("DISTRICT_NAME");
        this.roman = json.getString("DISTRICT_ROMAN");
        this.order = json.getInteger("DISTRICT_ORDER");
    }

    public JsonArray convert() {
        JsonArray params = new JsonArray();

        params.add(this.id);
        params.add(this.label);

        return params;
    }
}
