package cl.convenience.store.model.supplier;

import cl.convenience.store.model.commons.address.Address;
import cl.convenience.store.model.commons.person.Phone;
import cl.convenience.store.model.commons.person.Rut;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonObject;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Supplier {

    private Integer id;
    private String name;
    private Rut rut;
    private String details;
    private Address address;
    private Phone phone;

    public Supplier() {
        this.rut = new Rut();
        this.address = new Address();
        this.phone = new Phone();
    }

    public Supplier(Integer id) {
        this();
        this.id = id;
    }

    public Supplier(JsonObject json) {
        this.id = json.getInteger("SUP_ID");
        this.name = json.getString("SUP_NAME");
        this.rut = new Rut(json);
        this.details = json.getString("SUP_OTHER_DETAILS");
        this.address = new Address(json.getInteger("SUP_ADD_ID"));
        this.phone = new Phone(json.getInteger("SUP_PHONE_ID"));
    }
}
