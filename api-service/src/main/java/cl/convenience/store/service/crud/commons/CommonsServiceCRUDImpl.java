package cl.convenience.store.service.crud.commons;

import cl.convenience.store.dao.CommonsDAO;
import cl.convenience.store.dao.JdbcRepositoryWrapper;
import cl.convenience.store.model.commons.address.Address;
import cl.convenience.store.model.commons.address.City;
import cl.convenience.store.model.commons.address.Commune;
import cl.convenience.store.model.commons.address.District;
import cl.convenience.store.model.commons.person.Gender;
import cl.convenience.store.model.commons.person.Phone;
import cl.convenience.store.model.search.AddressSearch;
import cl.convenience.store.model.search.PhoneSearch;
import io.vertx.core.Future;

import java.util.List;

public class CommonsServiceCRUDImpl implements CommonsServiceCRUD {

    private final CommonsDAO commonsDAO;

    public CommonsServiceCRUDImpl(final JdbcRepositoryWrapper jdbc) {
        this.commonsDAO = CommonsDAO.getInstance(jdbc);
    }

    @Override
    public Future<Address> getAddressById(final String addressId) {
        return commonsDAO.getAddressByID(addressId);
    }

    @Override
    public Future<List<Address>> searchAddress(AddressSearch search) {
        return this.commonsDAO.addressSearch(search);
    }

    @Override
    public Future<List<District>> getDistrictList(){
        return this.commonsDAO.getDistrictList();
    }

    @Override
    public Future<List<City>> getCitiesByDistrictId(String districtId){
        return this.commonsDAO.getCitiesByDistrictID(districtId);
    }

    @Override
    public Future<List<Commune>> getCommunesByCityId(String cityId){
        return this.commonsDAO.getCommunesByCityID(cityId);
    }

    @Override
    public Future<Gender> getGenderById(String genderId) {
        return this.commonsDAO.getGenderById(genderId);
    }

    @Override
    public Future<List<Gender>> getGenderList(){
        return this.commonsDAO.getGenderList();
    }

    @Override
    public Future<Phone> getPhoneById(String phoneId){
        return this.commonsDAO.getPhoneById(phoneId);
    }

    @Override
    public Future<List<Phone>> searchPhones(PhoneSearch search){
        return this.commonsDAO.phoneSearch(search);
    }

    @Override
    public Future<Integer> saveAddress(Address address) {
        return this.commonsDAO.saveAddress(address);
    }

    @Override
    public Future<Integer> savePhone(Phone phone) {
        return this.commonsDAO.savePhone(phone);
    }
}
