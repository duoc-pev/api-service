package cl.convenience.store.model.employee;

import io.vertx.core.json.JsonObject;
import lombok.Data;

@Data
public class PermissionGroup {

    private Integer id;
    private String name;
    private String description;

    public PermissionGroup(JsonObject json) {
        this.id = json.getInteger("PGROUP_ID");
        this.name = json.getString("PGROUP_NAME");
        this.description = json.getString("PGROUP_DESC");
    }
}
