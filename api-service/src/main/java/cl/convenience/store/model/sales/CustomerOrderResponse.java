package cl.convenience.store.model.sales;

import cl.convenience.store.model.commons.Count;
import lombok.Data;

import java.util.List;

@Data
public class CustomerOrderResponse {

    private Count count;
    private List<CustomerOrder> customerOrders;
}
