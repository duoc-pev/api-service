package cl.convenience.store.dao.query;

import cl.convenience.store.model.search.OrderSearch;
import cl.convenience.store.model.search.SupplierEmployeeSearch;
import cl.convenience.store.model.search.SupplierSearch;

import static cl.convenience.store.util.Constant.ADD;
import static cl.convenience.store.util.Constant.AND;
import static cl.convenience.store.util.Constant.FROM;
import static cl.convenience.store.util.Constant.LEFT_OUTER_JOIN;
import static cl.convenience.store.util.Constant.SELECT;
import static cl.convenience.store.util.Constant.WHERE;

public class QuerySupplierUtil {

    private QuerySupplierUtil() {
        throw new IllegalAccessError(QuerySupplierUtil.class.toString());
    }

    public static String getSupplierByIdSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectSupplierOnly());
        query.append(fromSupplierOnly());
        query.append(WHERE);
        query.append(" sup.sup_id = ? ");

        return query.toString();
    }

    public static String searchSupplierSql(SupplierSearch search) {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectSupplierOnly());
        query.append(fromSupplierOnly());
        query.append(searchWhereSupplier(search));

        return query.toString();
    }

    public static String getPositionListSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectPositionOnly());
        query.append(fromPositionOnly());

        return query.toString();
    }

    public static String searchSupplierEmployeesSql(SupplierEmployeeSearch search) {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectSupplierOnly());
        query.append(ADD);
        query.append(selectSupplierEmployeesOnly());
        query.append(ADD);
        query.append(selectPositionOnly());
        query.append(fromSupplierFull());

        query.append(WHERE);
        query.append(" se.SE_EMP_ID is not null order by se.SE_EMP_ID ASC ");

        return query.toString();
    }

    public static String getEmployeesBySupplierIdSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectSupplierOnly());
        query.append(ADD);
        query.append(selectSupplierEmployeesOnly());
        query.append(ADD);
        query.append(selectPositionOnly());
        query.append(fromSupplierFull());

        query.append(WHERE);
        query.append(" sup.sup_id = ? ");

        return query.toString();
    }

    public static String getEmployeeByIdSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectSupplierEmployeesOnly());
        query.append(ADD);
        query.append(selectPositionOnly());
        query.append(fromEmployeesFull());

        query.append(WHERE);
        query.append(" se.se_emp_id = ? ");

        return query.toString();
    }

    public static String getOrderStatusSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectSupplierOrderStatus());
        query.append(fromOrderStatus());

        return query.toString();
    }

    public static String getOrderStatusByIdSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectSupplierOrderStatus());
        query.append(fromOrderStatus());
        query.append(WHERE);
        query.append(" sos.so_status_id = ? ");

        return query.toString();
    }

    public static String getOrderListSql(OrderSearch search) {
        return getOrderFull().append(searchWhereOrder(search)).toString();
    }

    public static String getOrderByIdSql() {
        StringBuilder where = new StringBuilder();

        where.append(WHERE);
        where.append(" supo.so_id = ? ");

        return getOrderFull().append(where).toString();
    }

    public static String getOrderDetailByOrderIdSql() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectSupplierOrderDetail());

        query.append(WHERE);
        query.append(" sos.sod_id = ? ");

        return query.toString();
    }

    private static StringBuilder getOrderFull() {
        StringBuilder query = new StringBuilder();

        query.append(SELECT);
        query.append(selectSupplierOrderOnly());
        query.append(ADD);
        query.append(selectSupplierOnly());
        query.append(ADD);
        query.append(selectSupplierEmployeesOnly());
        query.append(ADD);
        query.append(selectPositionOnly());
        query.append(ADD);
        query.append(selectSupplierOrderDetail());
        query.append(fromSupplierOrderFull());

        return query;
    }

    private static StringBuilder selectSupplierOnly() {
        StringBuilder query = new StringBuilder();

        query.append(" sup.sup_id, ");
        query.append(" sup.sup_name, ");
        query.append(" sup.sup_rut as RUT_PREFIX, ");
        query.append(" sup.sup_rut_dv as RUT_DV, ");
        query.append(" sup.sup_other_details, ");
        query.append(" sup.sup_add_id, ");
        query.append(" sup.sup_phone_id ");

        return query;
    }

    private static StringBuilder selectSupplierEmployeesOnly() {
        StringBuilder query = new StringBuilder();

        query.append(" se.se_emp_id, ");
        query.append(" se.se_fname, ");
        query.append(" se.se_flastname, ");
        query.append(" se.se_slastname, ");
        query.append(" se.se_gender_id, ");
        query.append(" se.se_email, ");
        query.append(" se.se_phone_1_id, ");
        query.append(" se.se_phone_2_id ");

        return query;
    }

    private static StringBuilder selectPositionOnly() {
        StringBuilder query = new StringBuilder();

        query.append(" pos.pos_id, ");
        query.append(" pos.pos_name, ");
        query.append(" pos.pos_description ");

        return query;
    }

    private static StringBuilder selectSupplierOrderOnly() {
        StringBuilder query = new StringBuilder();

        query.append(" supo.so_id, ");
        query.append(" supo.so_branch_id, ");
        query.append(" supo.so_placed_date, ");
        query.append(" supo.so_received_date ");

        return query;
    }

    private static StringBuilder selectSupplierOrderStatus() {
        StringBuilder query = new StringBuilder();

        query.append(" sos.so_status_id, ");
        query.append(" sos.so_status_name, ");
        query.append(" sos.so_status_description, ");
        query.append(" sos.so_status_level ");

        return query;
    }

    private static StringBuilder selectSupplierOrderDetail() {
        StringBuilder query = new StringBuilder();

        query.append(" sod.sod_id, ");
        query.append(" sod.sod_sup_id as SUP_ID, ");
        query.append(" sod.sod_quantity, ");
        query.append(" sod.sod_prod_un_id as product_un_id, ");
        query.append(" sod.sod_product_id as product_id, ");
        query.append(" sod.sod_net_price, ");
        query.append(" sod.sod_tax_price, ");
        query.append(" round(sod.sod_net_price + sod.sod_tax_price) as sod_full_price ");

        return query;
    }

    private static StringBuilder fromSupplierOnly() {
        StringBuilder query = new StringBuilder();

        query.append(FROM);
        query.append(" suppliers sup ");

        return query;
    }

    private static StringBuilder fromPositionOnly() {
        StringBuilder query = new StringBuilder();

        query.append(FROM);
        query.append(" position pos ");

        return query;
    }

    private static StringBuilder fromSupplierFull() {
        StringBuilder query = new StringBuilder();

        query.append(FROM);
        query.append(" suppliers sup ");
        query.append(LEFT_OUTER_JOIN);
        query.append(" sup_employees se on (se.se_sup_id = sup.sup_id) ");
        query.append(LEFT_OUTER_JOIN);
        query.append(" position pos on (pos.pos_id = se.se_pos_id) ");

        return query;
    }

    private static StringBuilder fromEmployeesFull() {
        StringBuilder query = new StringBuilder();

        query.append(FROM);
        query.append(" sup_employees se ");
        query.append(LEFT_OUTER_JOIN);
        query.append(" position pos on (pos.pos_id = se.se_pos_id) ");

        return query;
    }

    private static StringBuilder searchWhereSupplier(SupplierSearch search) {
        StringBuilder where = new StringBuilder();
        where.append(WHERE);
        where.append(" sup.sup_id > 0 ");

        if (null != search.getSupplier().getName()) {
            where.append(AND);
            where.append(" sup.sup_name LIKE '").append(search.getSupplier().getName()).append("%' ");
        }

        if (null != search.getSupplier().getRut().getRutPrefix()) {
            where.append(AND);
            where.append(" sup.sup_rut LIKE '").append(search.getSupplier().getRut().getRutPrefix()).append("%' ");
        }

        if (null != search.getSupplier().getAddress().getId()) {
            where.append(AND);
            where.append(" sup.sup_add_id =").append(search.getSupplier().getAddress().getId());
        }

        if (null != search.getSupplier().getPhone().getPhoneId()) {
            where.append(AND);
            where.append(" sup.sup_phone_id =").append(search.getSupplier().getPhone().getPhoneId());
        }

        return where;
    }

    private static StringBuilder searchWhereOrder(OrderSearch search) {
        StringBuilder where = new StringBuilder();
        where.append(WHERE);
        where.append(" supo.so_id > 0 ");

        if (null != search.getOrder().getOrderStatus().getId()) {
            where.append(AND);
            where.append(" supo.so_status_id = ").append(search.getOrder().getOrderStatus().getId());
        }

        if (null != search.getOrder().getSupplier().getId()) {
            where.append(AND);
            where.append(" supo.so_sup_id = ").append(search.getOrder().getSupplier().getId());
        }

        return where;
    }

    private static StringBuilder fromOrderStatus() {
        StringBuilder query = new StringBuilder();

        query.append(FROM);
        query.append(" supplier_order_status sos ");

        return query;
    }


    private static StringBuilder fromSupplierOrderFull() {
        StringBuilder query = new StringBuilder();

        query.append(FROM);
        query.append(" supplier_orders supo ");
        query.append(LEFT_OUTER_JOIN);
        query.append(" supplier_order_status sos on (sos.so_status_id = supo.so_status_id) ");
        query.append(LEFT_OUTER_JOIN);
        query.append(" supplier_order_detail sod on (sod.sod_id = supo.so_id) ");
        query.append(LEFT_OUTER_JOIN);
        query.append(" suppliers sup on (sup.sup_id = supo.so_sup_id) ");

        return query;
    }

}
