package cl.convenience.store.model.search;

import cl.convenience.store.model.commons.Pagination;
import cl.convenience.store.model.employee.Employee;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeSearch {

    private Employee employee;
    private Pagination pagination;

    public EmployeeSearch() {
        this.employee = new Employee();
        this.pagination = new Pagination();
    }
}
