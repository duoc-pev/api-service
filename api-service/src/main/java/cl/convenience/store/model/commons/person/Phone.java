package cl.convenience.store.model.commons.person;

import cl.convenience.store.util.FormatUtil;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Phone {

    private Integer phoneId;
    private Integer countryCode;
    private Integer areaCode;
    private Integer number;
    private String formattedNumber;
    private Boolean hasWhatsapp;

    public Phone(JsonObject json) {
        this.phoneId = json.getInteger("PHONE_ID");
        this.countryCode = json.getInteger("PHONE_COUNTRY_CODE");
        this.areaCode = json.getInteger("PHONE_AREA_CODE");
        this.number = json.getInteger("PHONE_NUMBER");
        this.formattedNumber = FormatUtil.getFormattedPhoneNumber(json.getInteger("PHONE_COUNTRY_CODE"), json.getInteger("PHONE_AREA_CODE"), json.getInteger("PHONE_NUMBER"));
        this.hasWhatsapp = null != json.getString("PHONE_WHATSAPP") ? json.getString("PHONE_WHATSAPP").equalsIgnoreCase("Y") : null;
    }

    public Phone(Integer id) {
        this.phoneId = id;
    }

    public JsonArray inParamsConvert() {
        JsonArray params = new JsonArray();

        params.add(countryCode);
        params.add(areaCode);
        params.add(number);
        params.add(hasWhatsapp);

        return params;
    }

    public JsonArray outParamsConvert() {
        JsonArray params = new JsonArray();

        params.addNull().addNull().addNull().addNull().add("INTEGER");

        return params;
    }
}
