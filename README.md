# Java API Service

## Preparativos para poder compilar

Es necesario agregar a nuestro .m2 local de Maven el driver JDBC de Oracle y por ende se deberán seguir los siguientes pasos:

1- Ejecutar el siguiente comando apuntando al driver para instalarlo en el repo local y así pueda Gradle reconocerlo y referenciarlo en la compilación

```shell
mvn install:install-file -Dfile=src/main/resources/ojdbc7.jar -DgroupId=com.oracle -DartifactId=ojdbc7 -Dversion=12.1.0.1 -Dpackaging=jar
```

## Compilar y ejecutar local

Para poder compilar y correr el programa basta con ejecutar el archivo bash run.sh con el siguiente comando que se encuentra dentro del servicio:

```shell
./run.sh
```

## 32 Endpoints funcionando y 4 más por cargar datos y probar

###Customer

####GET 
- http://localhost:9006/api/convenience/v1/customer/:id

####POST
- http://localhost:9006/api/convenience/v1/customer/_search

###Commons

####GET 
- http://localhost:9006/api/convenience/v1/commons/address/:id
- http://localhost:9006/api/convenience/v1/commons/gender
- http://localhost:9006/api/convenience/v1/commons/gender/:id
- http://localhost:9006/api/convenience/v1/commons/district
- http://localhost:9006/api/convenience/v1/commons/city/:id
- http://localhost:9006/api/convenience/v1/commons/commune/:id
- http://localhost:9006/api/convenience/v1/commons/phone/:id

####POST
- http://localhost:9006/api/convenience/v1/commons/address/_search
- http://localhost:9006/api/convenience/v1/commons/phone/_search

###Employee

####GET
- http://localhost:9006/api/convenience/v1/employees/roles
- http://localhost:9006/api/convenience/v1/employees/roles/:id
- http://localhost:9006/api/convenience/v1/employees/groups
- http://localhost:9006/api/convenience/v1/employees/branch
- http://localhost:9006/api/convenience/v1/employees/branch/:id
- http://localhost:9006/api/convenience/v1/employees/employee/:id

####POST
- http://localhost:9006/api/convenience/v1/employees/employee/_search

###Product

####GET
- http://localhost:9006/api/convenience/v1/products/category
- http://localhost:9006/api/convenience/v1/products/category/:id
- http://localhost:9006/api/convenience/v1/products/unit
- http://localhost:9006/api/convenience/v1/products/unit/:id
- http://localhost:9006/api/convenience/v1/products/brand
- http://localhost:9006/api/convenience/v1/products/brand/:id
- http://localhost:9006/api/convenience/v1/products/product/:id

####POST
- http://localhost:9006/api/convenience/v1/products/_search

###Supplier

####GET
- http://localhost:9006/api/convenience/v1/suppliers/supplier/:id
- http://localhost:9006/api/convenience/v1/suppliers/supplier/employees/position
- http://localhost:9006/api/convenience/v1/suppliers/supplier/employees/employee/:id
- http://localhost:9006/api/convenience/v1/suppliers/supplier/orders/order/status

####POST
- http://localhost:9006/api/convenience/v1/suppliers/supplier/_search
- http://localhost:9006/api/convenience/v1/suppliers/supplier/employees/employee/_search