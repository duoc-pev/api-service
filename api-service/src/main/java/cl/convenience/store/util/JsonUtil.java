package cl.convenience.store.util;

import gherkin.deps.com.google.gson.Gson;
import gherkin.deps.com.google.gson.GsonBuilder;

public class JsonUtil {

    private JsonUtil() {
        throw new IllegalAccessError(JsonUtil.class.toString());
    }
    public static final Gson GSON;

    static {
        GsonBuilder gsonBuilder = new GsonBuilder();
        GSON = gsonBuilder.create();
    }
}
