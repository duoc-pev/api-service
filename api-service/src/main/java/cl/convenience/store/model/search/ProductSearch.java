package cl.convenience.store.model.search;

import cl.convenience.store.model.commons.Pagination;
import cl.convenience.store.model.product.Product;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductSearch {

    private Product product;
    private Pagination pagination;
}
