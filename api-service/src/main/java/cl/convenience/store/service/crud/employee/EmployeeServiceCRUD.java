package cl.convenience.store.service.crud.employee;

import cl.convenience.store.model.employee.BranchOffice;
import cl.convenience.store.model.employee.Employee;
import cl.convenience.store.model.employee.Permission;
import cl.convenience.store.model.employee.PermissionGroup;
import cl.convenience.store.model.employee.Role;
import cl.convenience.store.model.search.EmployeeSearch;
import io.vertx.core.Future;

import java.util.List;

public interface EmployeeServiceCRUD {

    Future<List<Employee>> searchEmployee(EmployeeSearch search);

    Future<Employee> getEmployeeById(String employeeId);

    Future<List<Role>> getRoleList();

    Future<List<Permission>> getPermissionByRoleId(String roleId);

    Future<List<PermissionGroup>> getPermissionGroups();

    Future<List<BranchOffice>> getBranchList();

    Future<BranchOffice> getBranchById(String branchId);
}
